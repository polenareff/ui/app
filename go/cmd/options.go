package main

import (
  "github.com/go-flutter-desktop/go-flutter"
  "github.com/go-flutter-desktop/plugins/path_provider"
  "github.com/nealwon/go-flutter-plugin-sqlite"
  file_picker "github.com/miguelpruivo/flutter_file_picker/go"
  "github.com/go-flutter-desktop/plugins/shared_preferences"
  "github.com/go-flutter-desktop/plugins/url_launcher"
)

var options = []flutter.Option{
  flutter.WindowInitialDimensions(1200, 800),
  flutter.AddPlugin(&path_provider.PathProviderPlugin{
    VendorName:      "jkl",
    ApplicationName: "polenareff-app",
  }),
  flutter.AddPlugin(sqflite.NewSqflitePlugin("jkl","polenareff-app")),
  flutter.AddPlugin(&file_picker.FilePickerPlugin{}),
  flutter.AddPlugin(&path_provider.PathProviderPlugin{
    VendorName:      "jkl",
    ApplicationName: "polenareff-app",
  }),
  flutter.AddPlugin(&shared_preferences.SharedPreferencesPlugin{
    VendorName:      "jkl",
    ApplicationName: "polenareff-app",
  }),
  flutter.AddPlugin(&url_launcher.UrlLauncherPlugin{}),
}
