module polenareff_app/go

go 1.13

require (
	github.com/go-flutter-desktop/go-flutter v0.35.1
	github.com/go-flutter-desktop/plugins/path_provider v0.3.3
	github.com/go-flutter-desktop/plugins/shared_preferences v0.4.3
	github.com/go-flutter-desktop/plugins/url_launcher v0.1.2
	github.com/miguelpruivo/flutter_file_picker/go v0.0.0-20191218104902-b68ee11c6ac1
	github.com/nealwon/go-flutter-plugin-sqlite v0.0.0-20190909095325-db6bdfd6b983
	github.com/pkg/errors v0.9.1
)
