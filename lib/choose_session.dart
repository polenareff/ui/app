import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:path/path.dart' as path;
import 'package:polenareff_app/models.dart';
import 'package:rxdart/rxdart.dart';

import 'backdrop_content.dart';
import 'consts.dart';
import 'diagnostics.dart';
import 'menu.dart';
import 'worker.dart';

// ignore: must_be_immutable
class ChooseSession extends StatefulWidget {
  ChooseSession() {
    sessions = getSessions();
  }

  @override
  State<StatefulWidget> createState() => _ChooseSessionState();

  static List<Session> getSessions() {
    List<Session> result = List();
    Directory sessionDirectory =
        Directory(path.join(Worker.writeResultsPath, "sessions"));
    if (!sessionDirectory.existsSync()) return [];
    for (FileSystemEntity entity in sessionDirectory.listSync()) {
      if (FileSystemEntity.isDirectorySync(entity.path)) {
        print(entity.path);
        try {
          Session addSession = Session();
          addSession.name = path.basename(entity.path);
          File metaFile = File(path.join(entity.path, "meta.json"));
          String metaFileContents = metaFile.readAsStringSync();
          var contentJSON = json.decode(metaFileContents);
          bool valid = contentJSON['info']['telemetry'];
          bool routing = contentJSON['info']['fake'];
          addSession.valid = valid;
          addSession.workingDirectory = entity.path;
          addSession.photos = List();
          addSession.routing = routing;
          Directory viewPhotosDirectory = Directory(entity.path);
          Map<String, Map<String, dynamic>> photosMap = Map();
          for (var s in contentJSON['photos']) {
            photosMap[s['name']] = s;
          }
          List<FileSystemEntity> entitiesList = viewPhotosDirectory.listSync();
          Function compare = (entity1, entity2) {
            String s1 = path.basenameWithoutExtension(entity1.path);
            String s2 = path.basenameWithoutExtension(entity2.path);
            return s1.compareTo(s2);
          };
          entitiesList.sort(compare);
          for (FileSystemEntity photoEntity in entitiesList) {
            if (path.extension(photoEntity.path) == ".json" ||
                path.basenameWithoutExtension(photoEntity.path) ==
                    "thumbnail" ||
                path.basenameWithoutExtension(photoEntity.path) ==
                    "combination") continue;
            SessionPhoto sessionPhoto = SessionPhoto();
            sessionPhoto.name = path.basename(photoEntity.path);
            sessionPhoto.alt = photosMap[sessionPhoto.name]['alt'].toDouble();
            sessionPhoto.lat = photosMap[sessionPhoto.name]['lat'].toDouble();
            sessionPhoto.lng = photosMap[sessionPhoto.name]['lng'].toDouble();
            sessionPhoto.yaw = photosMap[sessionPhoto.name]['yaw'].toDouble();
            addSession.photos.add(sessionPhoto);
          }
          if (addSession.photos.length > 0) result.add(addSession);
        } catch (e) {
          print(e);
        }
      }
    }
    return result;
  }

  List<Session> sessions = List();

  static void openDiagnostics(Session session, BuildContext context,
      {replace = false}) {
    Worker.combinationResultControllers[session.name] = BehaviorSubject();
    Worker.animationResultControllers[session.name] = BehaviorSubject();
    Worker.hydrationResultControllers[session.name] = BehaviorSubject();
    Worker.comparisonController = BehaviorSubject();
    Worker.comparingSession = null;
    Worker.makeSessionRequests(session);
    if (replace)
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => DiagnosticsPage(session),
        ),
      );
    else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DiagnosticsPage(session),
        ),
      );
    }
  }
}

class _ChooseSessionState extends State<ChooseSession> {
  void showEditDialog(int index) {
    File metaFile =
        File(path.join(widget.sessions[index].workingDirectory, "meta.json"));
    String metaContents = metaFile.readAsStringSync();
    Map<String, dynamic> metaParsed = json.decode(metaContents);
    setState(() {
      Worker.readDroneInfo(metaParsed['drone']);
    });
    showDialog(
        context: context,
        builder: (context) {
          return Theme(
              data: ThemeData(
                dialogBackgroundColor: Consts.COLOR_MAIN,
              ),
              child: AlertDialog(
                title: Text(
                  "Editing session",
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                content: BackdropContent.dialog(widget.sessions[index].name),
                actions: <Widget>[
                  FlatButton(
                      child: Text(
                        "Save",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        metaParsed['drone'] = Worker.getDroneParams();
                        metaFile.writeAsStringSync(json.encode(metaParsed));
                        Directory sessionDirectory =
                            Directory(widget.sessions[index].workingDirectory);
                        sessionDirectory.renameSync(path.join(
                            sessionDirectory.parent.path,
                            BackdropContent.getNewName()));
                        setState(() {
                          widget.sessions = ChooseSession.getSessions();
                        });
                        Navigator.of(context).pop();
                      }),
                  FlatButton(
                    child: Text(
                      "Close",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ],
              ));
        });
  }

  Widget _getBody() {
    return getSessionList(widget.sessions);
  }

  Widget getSessionList(List<Session> sessions) {
    if (sessions.length == 0) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                child: Image.asset("assets/empty.png"),
              ),
              SizedBox(height: 20),
              Text(
                "You haven't created any session yet!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    child: Text("Import session"),
                    onPressed: () {
                      MenuPage.openImportPage(context, replace: true);
                    },
                  ),
                  SizedBox(width: 30),
                  RaisedButton(
                    child: Text("Build route"),
                    onPressed: () {
                      MenuPage.openRoutingPage(context, replace: true);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
    return ListView.separated(
      separatorBuilder: (context, index) => Padding(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: Divider(
          color: Color.fromARGB(255, 200, 200, 200),
        ),
      ),
      itemCount: sessions.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            ChooseSession.openDiagnostics(sessions[index], context,
                replace: false);
          },
          child: Container(
            height: 220,
            color: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.only(bottom: 10, top: 10),
              child: Slidable(
                actionPane: SlidableDrawerActionPane(),
                actionExtentRatio: 0.25,
                child: Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 7, 0, 7),
                      child: Image.file(
                        File(
                          path.join(sessions[index].workingDirectory,
                              "thumbnail.jpg"),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Spacer(flex: 1),
                    Flexible(
                      flex: 2,
                      fit: FlexFit.tight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(height: 70),
                          Text(
                            sessions[index].name,
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(height: 15),
                          Text("${sessions[index].photos.length} photos"),
                        ],
                      ),
                    ),
                    Spacer(flex: 1),
                  ],
                ),
                actions: !sessions[index].routing
                    ? <Widget>[
                        IconSlideAction(
                          caption: 'Edit',
                          color: Colors.blue,
                          icon: Icons.edit,
                          onTap: () {
                            showEditDialog(index);
                          },
                        ),
                      ]
                    : [],
                secondaryActions: <Widget>[
                  IconSlideAction(
                      caption: 'Delete',
                      color: Colors.red,
                      icon: Icons.delete,
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: Text("Deleting session"),
                                content: Text(
                                    "Are you sure you want to delete this session?"
                                    " After deleting session can't be restored."),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text("Delete"),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      Directory sessionDir = Directory(
                                          sessions[index].workingDirectory);
                                      sessionDir
                                          .delete(recursive: true)
                                          .then((value) {
                                        setState(() {
                                          widget.sessions =
                                              ChooseSession.getSessions();
                                        });
                                      });
                                    },
                                  ),
                                  FlatButton(
                                    child: Text("Close"),
                                    onPressed: () =>
                                        Navigator.of(context).pop(),
                                  ),
                                ],
                              );
                            });
                      }),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: UsefulFunctions.getAppTitle("Полеnareff"),
        centerTitle: true,
      ),
      body: _getBody(),
    );
  }
}
