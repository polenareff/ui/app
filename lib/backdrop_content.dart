import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'consts.dart';
import 'messenger.dart';
import 'models.dart';
import 'worker.dart';

class BackdropContent extends StatefulWidget {
  BackdropContent();

  @override
  State<StatefulWidget> createState() => _BackdropContentState();
  bool isDialog = false;
  static String name = "";
  static TextEditingController sessionNameController = TextEditingController();

  factory BackdropContent.dialog(String name) {
    BackdropContent content = BackdropContent();
    content.isDialog = true;
    BackdropContent.name = name;
    BackdropContent.sessionNameController.text = name;
    return content;
  }

  static String getNewName() {
    return BackdropContent.sessionNameController.text;
  }
}

class _BackdropContentState extends State<BackdropContent> {
  @override
  Widget build(BuildContext context) {
    if (!widget.isDialog) {
      return SizedBox(
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.8 - 50,
              child:
                  ListView(shrinkWrap: true, children: _getBackdropElements()),
            ),
          ],
        ),
      );
    } else {
      return Container(
          width: MediaQuery.of(context).size.width,
          child: ListView(shrinkWrap: true, children: _getBackdropElements()));
    }
  }

  void _showDeletePresetDialog(int index, String name) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Deleting preset"),
        content: Text("Are you sure you want to delete preset \"$name\"?"),
        actions: <Widget>[
          FlatButton(
            child: Text("Delete"),
            onPressed: () {
              Worker.deleteDronePreset(index - Consts.DRONE_PRESETS.length);
              Drone drone = Worker.getDronePresets()[0];
              setState(() {
                Worker.setDroneName(drone.name);
                Worker.droneBattery.setValue(drone.battery);
                Worker.droneBatteryChangeKm.setValue(drone.batteryChangeKm);
                Worker.droneBatteryChangePhoto
                    .setValue(drone.batteryChangePhoto);
                Worker.droneFocusDistance.setValue(drone.focusDistance);
                Worker.droneMaxHeight.setValue(drone.maxHeight);
              });
              Navigator.of(context).pop();
              _showDronePresetDialog();
            },
          ),
          FlatButton(
            child: Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
              _showDronePresetDialog();
            },
          )
        ],
      ),
    );
  }

  void _showDronePresetDialog() {
    List<Drone> dronePresets = Worker.getDronePresets();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Theme(
            data: ThemeData(
              dialogBackgroundColor: Colors.white,
            ),
            child: SimpleDialog(
              title: Text("Choose drone preset"),
              children: dronePresets
                  .asMap()
                  .map((index, drone) {
                    return MapEntry(
                      index,
                      GestureDetector(
                        onLongPress: () {
                          if (index >= Consts.DRONE_PRESETS.length) {
                            Navigator.of(context).pop();
                            _showDeletePresetDialog(index, drone.name);
                          }
                        },
                        child: SimpleDialogOption(
                          child: Text(
                            drone.name,
                          ),
                          onPressed: () {
                            setState(() {
                              Worker.setDroneName(drone.name);
                              Worker.droneBattery.setValue(drone.battery);
                              Worker.droneBatteryChangeKm
                                  .setValue(drone.batteryChangeKm);
                              Worker.droneBatteryChangePhoto
                                  .setValue(drone.batteryChangePhoto);
                              Worker.droneFocusDistance
                                  .setValue(drone.focusDistance);
                              Worker.droneMaxHeight.setValue(drone.maxHeight);
                            });
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    );
                  })
                  .values
                  .toList(),
            ));
      },
    );
  }

  Widget _getBackdropSlider(String picturePath, String beginText,
      String endText, BackDropElementValue bdev,
      {double imageSize = 50.0, double boxSize = 50.0, Widget additional}) {
    double w = MediaQuery.of(context).size.width;
    return DefaultTextStyle(
      style: TextStyle(
          fontWeight: FontWeight.w900,
          fontFamily: 'Roboto',
          fontFamilyFallback: ['Roboto']),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
              height: imageSize,
              width: imageSize,
              child: Image.asset(picturePath)),
          additional ??
              SizedBox(
                width: 100/1920*w,
              ),
          (additional != null)
              ? SizedBox(
                  width: max(10, 100/1920*w-50),
                )
              : Container(),
          Slider(
            min: bdev.minValue.toDouble(),
            max: bdev.maxValue.toDouble(),
            value: bdev.value.toDouble(),
            divisions: bdev.divisions,
            activeColor: Colors.white,
            inactiveColor: Colors.white,
            onChanged: (double value) async {
              setState(() {
                bdev.setValue(value.truncate());
              });
            },
          ),
          SizedBox(
            width: 100/1920*w,
          ),
          SizedBox(
              width: boxSize, child: Text("$beginText${bdev.value}$endText")),
        ],
      ),
    );
  }

  List<Widget> _getBackdropElements() {
    double w = MediaQuery.of(context).size.width;
    return [
      SizedBox(height: 5),
      widget.isDialog
          ? Center(
              child: SizedBox(
                width: 490,
                child: TextField(
                  controller: BackdropContent.sessionNameController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Session name',
                    hintStyle: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 200, 200, 200),
                      ),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 200, 200, 200),
                      ),
                    ),
                  ),
                  onChanged: (text) {},
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w900,
                    fontSize: 20,
                  ),
                ),
              ),
            )
          : Container(
              height: 0,
            ),
      widget.isDialog ? SizedBox(height: 10) : SizedBox(height: 5),
      Container(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(width: 190/1920*w),
            Center(
              child: SizedBox(
                width: 200/1920*w,
                child: TextField(
                  controller: Worker.droneNameController,
                  decoration: InputDecoration(
                    hintText: 'Drone name',
                    hintStyle: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 200, 200, 200),
                      ),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromARGB(255, 200, 200, 200),
                      ),
                    ),
                  ),
                  onChanged: (text) {
                    Worker.prefs.setString(Consts.PREFS_DRONE_NAME, text);
                  },
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w900,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            SizedBox(width: 90/1920*w),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: IconButton(
                icon: Icon(Icons.more_vert),
                color: Colors.white,
                onPressed: () => _showDronePresetDialog(),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: IconButton(
                icon: Icon(Icons.save),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => Theme(
                      data: ThemeData(
                        dialogBackgroundColor: Colors.white,
                      ),
                      child: AlertDialog(
                        title: Text("Saving drone preset"),
                        content: Text(
                            "Are you sure you want to save a new drone preset "
                            "with name "
                            "\"${Worker.droneNameController.text.toString()}\"? "
                            "You might have created it already."),
                        actions: <Widget>[
                          FlatButton(
                            child: Text("Save"),
                            onPressed: () {
                              Worker.saveNewDronePreset(Drone(
                                  name: Worker.droneNameController.text
                                      .toString(),
                                  maxHeight: Worker.droneMaxHeight.value,
                                  battery: Worker.droneBattery.value,
                                  focusDistance:
                                      Worker.droneFocusDistance.value,
                                  batteryChangeKm:
                                      Worker.droneBatteryChangeKm.value,
                                  batteryChangePhoto:
                                      Worker.droneBatteryChangePhoto.value));
                              Navigator.pop(context);
                            },
                          ),
                          FlatButton(
                            child: Text("Close"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      ),
                    ),
                  );
                },
                color: Colors.white,
                tooltip: "Save preset",
              ),
            ),
          ],
        ),
      ),
      SizedBox(
        height: 15,
      ),
      _getBackdropSlider(
        'assets/focus.png',
        '',
        ' mm',
        Worker.droneFocusDistance,
      ),
      SizedBox(height: 10),
      _getBackdropSlider(
        'assets/height.png',
        '',
        ' m',
        Worker.droneMaxHeight,
      ),
      SizedBox(height: 10),
      _getBackdropSlider(
        'assets/overlay.png',
        '',
        ' %',
        Worker.dronePhotoOverlay,
      ),
      SizedBox(height: 10),
      _getBackdropSlider(
        'assets/battery.png',
        '',
        ' units',
        Worker.droneBattery,
      ),
      SizedBox(height: 10),
      _getBackdropSlider(
        'assets/battery-km.png',
        '',
        ' units/km',
        Worker.droneBatteryChangeKm,
        boxSize: 50,
      ),
      SizedBox(height: 10),
      _getBackdropSlider(
        'assets/battery-photo.png',
        '',
        ' units/photo',
        Worker.droneBatteryChangePhoto,
      ),
      SizedBox(
        height: 20,
      ),
      _getBackdropSlider(
        'assets/battery-pack.png',
        '',
        ' packs',
        Worker.dronePacksAmount,
        additional: Checkbox(
          onChanged: (bool value) {
            setState(() {
              value
                  ? Worker.droneManyPacks.setValue(1)
                  : Worker.droneManyPacks.setValue(0);
            });
          },
          value: (Worker.droneManyPacks.value == 1) ? true : false,
        ),
      ),
      SizedBox(
        height: 20,
      ),
      Worker.isInDebugMode
          ? TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Server address (WITHOUT PORT!)',
              ),
              onChanged: (String s) {
                if (s.startsWith("http://"))
                  Consts.SERVER_ADDRESS = s;
                else
                  Consts.SERVER_ADDRESS = "http://$s";
              },
            )
          : SizedBox(),
    ];
  }
}
