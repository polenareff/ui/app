import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'consts.dart';
import 'menu.dart';

class PoleApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Полеnareff',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: Consts.COLOR_MAIN,
        ),
        primarySwatch: MaterialColor(Consts.COLOR_MAIN_INT, {
          50: Consts.COLOR_MAIN,
          100: Consts.COLOR_MAIN,
          200: Consts.COLOR_MAIN,
          300: Consts.COLOR_MAIN,
          400: Consts.COLOR_MAIN,
          500: Consts.COLOR_MAIN,
          600: Consts.COLOR_MAIN,
          700: Consts.COLOR_MAIN,
          800: Consts.COLOR_MAIN,
          900: Consts.COLOR_MAIN,
        }),
      ),
      home: MenuPage(title: 'Полеnareff'),
      debugShowCheckedModeBanner: false,
    );
  }
}