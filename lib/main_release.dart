import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'root.dart';
import 'worker.dart';


void main() async{
	WidgetsFlutterBinding.ensureInitialized();
	Worker.rewriteDebug = "release";
  Worker.prefs = await SharedPreferences.getInstance();
  Directory dir = await getExternalStorageDirectory();
  Worker.writeResultsPath = dir.path;
  runApp(PoleApp());
}


