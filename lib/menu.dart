import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:path_provider/path_provider.dart';
import 'package:polenareff_app/models.dart';
import 'package:url_launcher/url_launcher.dart';

import 'choose_session.dart';
import 'consts.dart';
import 'import_session.dart';
import 'routing.dart';
import 'worker.dart';

class MenuPage extends StatelessWidget {
  static void openImportPage(BuildContext context, {bool replace = false}) {
    Worker.isImportCompressingPhotos = false;
    Worker.importList = List();
    Worker.importMetaFile = null;
    Worker.importFileList = List();
    Worker.importCurrentIndex = 0;
    if (!replace)
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ImportSession()),
      );
    else
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => ImportSession()),
      );
  }

  static void openChoosingPage(BuildContext context, {bool replace = false}) {
    if (!replace)
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ChooseSession()),
      );
    else
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => ImportSession()),
      );
  }

  static void openRoutingPage(BuildContext context, {bool replace = false}) {
    Worker.mappingMemory[0] = Consts.STATE_ADDING;
    Worker.mappingMemory[1] = Marker(width: -1);
    Worker.mappingMemory[2] = List();
    Worker.mappingMemory[3] = List();
    Worker.mappingMemory[4] = List();
    Worker.mappingMemory[5] = MapController();
    Worker.mappingMemory[6] = StreamController<LatLng>.broadcast();
    Worker.routingCanCreateMarker = true;
    if (!replace)
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RoutingPage()),
      );
    else
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => RoutingPage()),
      );
  }

  static Future<File> copyAsset(String name) async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File tempFile = File('$tempPath/$name.pdf');
    ByteData bd = await rootBundle.load('assets/$name.pdf');
    await tempFile.writeAsBytes(bd.buffer.asUint8List(), flush: true);
    return tempFile;
  }

  static void openHelpPage(BuildContext context) async {
    copyAsset("userdoc").then((File file1) {
      copyAsset("techdoc").then((File file2) {
        launch(file1.path);
        launch(file2.path);
      });
    });
  }

  final String title;

  MenuPage({this.title});

  Widget _getDashboardItem(
      Widget icon, String text, Function onPressed, double w) {
    return FlatButton(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          icon,
          SizedBox(
            width: w/1500*300,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: max(15, w / 1500 * 20)),
            ),
          ),
        ],
      ),
      onPressed: onPressed,
    );
  }

  Widget _getBody(context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Wrap(
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: w / 1920 * 60,
              runSpacing: w / 1920 * 60,
              children: <Widget>[
                _getDashboardItem(
                  Icon(
                    Icons.list,
                    size: min(h / 2, w / 2) * 0.4,
                  ),
                  "Choose session",
                  () => MenuPage.openChoosingPage(context),
                  w,
                ),
                _getDashboardItem(
                  SizedBox(
                    height: min(h / 2, w / 2) * 0.4,
                    child: Image.asset("assets/import-black.png"),
                  ),
                  "Import session",
                  () => MenuPage.openImportPage(context),
                  w,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Wrap(
              spacing: w / 1920 * 60,
              runSpacing: w / 1920 * 60,
              children: [
                _getDashboardItem(
                  Icon(
                    Icons.timeline,
                    size: min(h / 2, w / 2) * 0.4,
                  ),
                  "Build route",
                  () => MenuPage.openRoutingPage(context),
                  w,
                ),
                _getDashboardItem(
                  Icon(
                    Icons.help,
                    size: min(h / 2, w / 2) * 0.4,
                  ),
                  "Help",
                  () => MenuPage.openHelpPage(context),
                  w,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Consts.COLOR_MAIN,
        title: UsefulFunctions.getAppTitle(title),
        centerTitle: true,
      ),
      body: _getBody(context),
    );
  }
}
