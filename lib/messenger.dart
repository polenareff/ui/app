import 'package:flutter/material.dart';

import 'worker.dart';

class Messenger {
  static void makeMessage(String message,
      GlobalKey<ScaffoldState> scaffoldKey,
      {String buttonMessage,
      Function onButtonPressed,
      Duration duration = const Duration(seconds: 3),
      Function onClosed,
      bool removePrevious = true,
      bool constant = false,}) {
    if (removePrevious)
      scaffoldKey?.currentState
          ?.removeCurrentSnackBar(reason: SnackBarClosedReason.remove);
    scaffoldKey?.currentState
        ?.showSnackBar(
          SnackBar(
            content: Text(message),
            duration: duration,
            action: (buttonMessage != null)
                ? SnackBarAction(
                    label: buttonMessage, onPressed: onButtonPressed)
                : null,
          ),
        )
        ?.closed
        ?.then((reason) => onClosed != null ? onClosed(reason) : {});
  }
}
