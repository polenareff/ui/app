import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';
import 'package:path/path.dart' as path;
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:image/image.dart' as imglib;
import 'backdrop.dart';
import 'backdrop_content.dart';
import 'choose_session.dart';
import 'messenger.dart';
import 'models.dart';
import 'worker.dart';

// ignore: must_be_immutable
class ImportSession extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ImportSessionState();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
}

class _ImportSessionState extends State<ImportSession> {
  PhotoViewController controller = PhotoViewController()..scale = 1;
  double minScale = 1;
  double maxScale = 5;
  double scaleChange = 0.25;

  void _pointerSignal(PointerSignalEvent details) {
    if (debugDefaultTargetPlatformOverride == TargetPlatform.fuchsia) {
      PointerScrollEvent event = details;
      if (event.scrollDelta.dy > 10) {
        double newScale = controller.scale - scaleChange;
        newScale = math.min(newScale, maxScale);
        newScale = math.max(newScale, minScale);
        controller.scale = newScale;
      } else if (event.scrollDelta.dy < -10) {
        double newScale = controller.scale + scaleChange;
        newScale = math.min(newScale, maxScale);
        newScale = math.max(newScale, minScale);
        controller.scale = newScale;
      }
      print(controller.scale);
    }
  }

  Widget _getBody() {
    if (Worker.isImportCompressingPhotos) {
      return Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Spacer(flex: 1),
            CircularProgressIndicator(),
            SizedBox(height: 10),
            Text(
              "Compressing images...",
              style: TextStyle(
                fontSize: 16,
              ),
            ),
            Spacer(flex: 1),
          ],
        ),
      );
    }
    if (Worker.importList.length == 0) {
      //return Text("Please press '+'");
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                child: Image.asset("assets/import.png"),
              ),
              SizedBox(height: 20),
              Text(
                'Please, choose some files first.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24,
                ),
              )
            ],
          ),
        ),
      );
    }

    return Listener(
      onPointerSignal: _pointerSignal,
      child: PhotoViewGallery.builder(
        builder: (BuildContext context, int index) {
          return PhotoViewGalleryPageOptions(
            imageProvider: Image.memory(Worker.importList[index]).image,
            initialScale: PhotoViewComputedScale.covered * 0.9,
            minScale: PhotoViewComputedScale.covered * 0.9,
            maxScale: PhotoViewComputedScale.covered * 5,
            controller: controller,
          );
        },
        itemCount: Worker.importList.length,
        scrollDirection: Axis.horizontal,
        backgroundDecoration: BoxDecoration(color: Colors.white),
        pageController: PageController(
          initialPage: Worker.importCurrentIndex % Worker.importList.length,
        ),
        onPageChanged: (index) {
          Worker.importCurrentIndex = index;
        },
      ),
    );
  }

  void _showCreateSessionDialog() {
    TextEditingController _textFieldController = TextEditingController();
    showDialog(
      context: context,
      builder: (context1) {
        return AlertDialog(
          title: Text("Choose session name"),
          content: TextField(
            controller: _textFieldController,
            decoration: InputDecoration(hintText: "Enter name here"),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Import"),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  Worker.isImportNetworking = true;
                });
                Messenger.makeMessage(
                    "Importing session...", widget.scaffoldKey,
                    duration: Duration(seconds: 1000));
                Worker.importSession(
                  _textFieldController.text.toString(),
                  onReady: (session) {
                    setState(() {
                      Worker.isImportNetworking = false;
                    });
                    Messenger.makeMessage(
                              "The session was imported successfully!",
                              widget.scaffoldKey,
                              duration: Duration(seconds: 1000),
                              buttonMessage: "Open diagnostics",
                              onButtonPressed: () =>
                                  ChooseSession.openDiagnostics(
                                      session, context,
                                      replace: true),
                    );
                  },
                  onError: (response) {
                    setState(() {
                      Worker.isImportNetworking = false;
                    });
                    Messenger.makeMessage(
                        "An error occured during importing session",
                        widget.scaffoldKey);
                  },
                  valid: (Worker.importMetaFile != null),
                );
              },
            ),
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  Widget _getFAB() {
    if (Worker.isImportCompressingPhotos || Worker.isImportNetworking)
      return null;
    if (Worker.importList.length == 0)
      return FloatingActionButton.extended(
        label: Text("Photos"),
        icon: Icon(Icons.add),
        onPressed: () => _addPhotos(),
      );
    else
      return FloatingActionButton.extended(
          label: Text("Import"),
          icon: Icon(Icons.done),
          onPressed: () => _showCreateSessionDialog());
  }

  void _addPhotos() async {
    List<File> fl = List();
    fl = await FilePicker.getMultiFile(type: FileType.ANY);
    if (fl == null) return;
    setState(() {
      Worker.isImportCompressingPhotos = true;
    });
    Function cmp = (File file1, File file2) {
      return file1.path.compareTo(file2.path);
    };
    fl.sort(cmp);
    compute(Worker.compressParsingPhotos, fl).then((list) {
      setState(() {
        Worker.importMetaFile = list[0];
        Worker.importList = list[1];
        Worker.importFileList = list[2];
        Worker.isImportCompressingPhotos = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BackdropScaffold(
      backLayer: BackdropContent(),
      title: UsefulFunctions.getAppTitle("Полеnareff"),
      headerHeight: MediaQuery.of(context).size.height / 5,
      headerColor: Colors.white.withOpacity(0.6),
      leadingWidget: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Navigator.pop(context),
        tooltip: "Go back, in menu",
      ),
      frontLayer: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Scaffold(
          key: widget.scaffoldKey,
          body: _getBody(),
          floatingActionButton: AnimatedSwitcher(
              duration: Duration(milliseconds: 200), child: _getFAB()),
        ),
      ),
    );
  }
}
