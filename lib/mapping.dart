import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:latlong/latlong.dart';
import 'package:rxdart/rxdart.dart';
import 'package:screenshot/screenshot.dart';

import 'consts.dart';
import 'models.dart';
import 'worker.dart';

// ignore: must_be_immutable

class RouteMap extends StatefulWidget {
  static final Future<Secret> secret =
      SecretLoader(secretPath: "secrets.json").load();
  double zoom = Consts.MAP_VALUE_ZOOM;

  final int stateIndex;
  final int droneMarkerIndex;
  final int addMarkersIndex;
  final int resultPointsIndex;
  final int resultPhotoPointsIndex;
  final int mapControllerIndex;
  final int dronePosControllerIndex;
  final bool showPhotos;

  RouteMap(
      this.stateIndex,
      this.droneMarkerIndex,
      this.addMarkersIndex,
      this.resultPointsIndex,
      this.resultPhotoPointsIndex,
      this.mapControllerIndex,
      this.dronePosControllerIndex,
      {this.showPhotos = false});

  void mapZoomIn({double customZoom = 0.1, bool bs = true}) {
    if (bs) {
      _mapZoomIn(customZoom);
      _mapZoomOut(customZoom);
      _mapZoomIn(customZoom);
      //Пошли нахуй, пишу код как хочу
    } else {
      _mapZoomIn(customZoom);
    }
  }

  void mapZoomOut({double customZoom = 0.1, bool bs = true}) {
    _mapZoomOut(customZoom);
  }

  void _mapZoomIn(double changeZoom) {
    double zoom = Worker.mappingMemory[mapControllerIndex].zoom;
    zoom += changeZoom;
    zoom = min(zoom, Consts.MAP_MAX_ZOOM);
    zoom = max(zoom, Consts.MAP_MIN_ZOOM);
    Worker.mappingMemory[mapControllerIndex]
        .move(Worker.mappingMemory[mapControllerIndex].center, zoom);
    //routingZoom = Worker.mappingMemory[mapControllerIndex].zoom;
  }

  void _mapZoomOut(double changeZoom) {
    double zoom = Worker.mappingMemory[mapControllerIndex].zoom;
    zoom -= changeZoom;
    zoom = min(zoom, Consts.MAP_MAX_ZOOM);
    zoom = max(zoom, Consts.MAP_MIN_ZOOM);
    Worker.mappingMemory[mapControllerIndex]
        .move(Worker.mappingMemory[mapControllerIndex].center, zoom);
    //routingZoom = Worker.mappingMemory[mapControllerIndex].zoom;
  }

  @override
  State<StatefulWidget> createState() => _RouteMapState();
}

class _RouteMapState extends State<RouteMap> {
  int lastRightClickMs = -1000;
  LatLng dronePos;

  void _pointerDown(PointerDownEvent details) {
    if (debugDefaultTargetPlatformOverride == TargetPlatform.fuchsia) {
      if (details.buttons == 2) {
        if (DateTime.now().millisecondsSinceEpoch - lastRightClickMs <= 300) {
          setState(() {
            Worker.mappingMemory[widget.mapControllerIndex].move(
                Worker.mappingMemory[widget.mapControllerIndex].center,
                Worker.mappingMemory[widget.mapControllerIndex].zoom - 0.9);
          });
          lastRightClickMs = -1000;
        } else
          lastRightClickMs = DateTime.now().millisecondsSinceEpoch;
      }
    }
  }

  void _pointerSignal(PointerSignalEvent details) {
    if (debugDefaultTargetPlatformOverride == TargetPlatform.fuchsia) {
      PointerScrollEvent event = details;
      if (event.scrollDelta.dy > 10) {
        setState(() {
          widget.mapZoomOut(
            customZoom: 0.25,
            bs: false,
          );
        });
      } else if (event.scrollDelta.dy < -10) {
        widget.mapZoomIn(
          customZoom: 0.25,
          bs: false,
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (Worker.mappingMemory[widget.stateIndex] != Consts.STATE_MAKING_PHOTO)
      return StreamBuilder(
        stream: Worker.mappingMemory[widget.dronePosControllerIndex].stream,
        builder: (context, snapshot) {
          dronePos = snapshot.data;
          return Listener(
            onPointerDown: _pointerDown,
            onPointerSignal: _pointerSignal,
            child: _buildMap(),
          );
        },
      );
    else {
      return Screenshot(
        controller: Worker.mappingScreenShotController,
        child: AbsorbPointer(
          child: _buildMap(),
        ),
      );
    }
  }

  Widget _buildMap() {
    return (Worker.isInDebugMode) ? _buildDebugMap() : _buildReleaseMap();
  }

  Widget _buildDebugMap() {
    return FlutterMap(
      options: _getMapOptions(),
      mapController: Worker.mappingMemory[widget.mapControllerIndex],
      layers: [
        TileLayerOptions(
          urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
          subdomains: ['a', 'b', 'c'],
        ),
        PolylineLayerOptions(
          polylines: _getPolylines(),
        ),
        MarkerLayerOptions(
          markers: _getMarkers(),
        ),
      ],
    );
  }

  Widget _buildReleaseMap() {
    return FutureBuilder(
        future: RouteMap.secret,
        builder: (context, AsyncSnapshot<Secret> snapshot) {
          if (snapshot.hasError) {
            return Text("Error happened on loading map");
          }
          if (snapshot.hasData) {
            return FlutterMap(
              mapController: Worker.mappingMemory[widget.mapControllerIndex],
              options: _getMapOptions(),
              layers: [
                new TileLayerOptions(
                  urlTemplate: "https://api.mapbox.com/styles/v1/chopikus/"
                      "ck0fd2yrr1i7q1cpkr86wwvpx/tiles/256/{z}/{x}/{y}@2x"
                      "?access_token="
                      "${snapshot.data.mapBoxKey}",
                  additionalOptions: {
                    'accessToken': snapshot.data.mapBoxKey,
                    'id': 'mapbox.mapbox-streets-v7'
                  },
                ),
                PolylineLayerOptions(
                  polylines: _getPolylines(),
                ),
                MarkerLayerOptions(
                  markers: _getMarkers(),
                ),
              ],
            );
          }
          return CircularProgressIndicator();
        });
  }

  void _deleteMarker(MarkerWithKey markerWithKey) {
    int ind = -1;
    for (int i = 0;
        i < Worker.mappingMemory[widget.addMarkersIndex].length;
        i++) {
      if (Worker.mappingMemory[widget.addMarkersIndex][i].key ==
          markerWithKey.key) {
        ind = i;
        break;
      }
    }
    if (ind != -1) {
      setState(() {
        Worker.mappingMemory[widget.addMarkersIndex].removeAt(ind);
      });
    }
  }

  void onMapTap(LatLng where) {
    if (Worker.mappingMemory[widget.stateIndex] != Consts.STATE_ADDING ||
        !Worker.routingCanCreateMarker) {
      //смело может идти нахуй!
      return;
    }
    MarkerWithKey markerWithKey = MarkerWithKey();
    Marker marker = Marker(
      width: 45.0,
      height: 45.0,
      anchorPos: AnchorPos.align(AnchorAlign.top),
      point: where,
      builder: (context) => new Container(
        child: GestureDetector(
          child: Icon(
            Icons.location_on,
            color: Colors.blue,
            size: 45,
          ),
          onTap: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("Delete point?"),
                    content: Text("Are you sure you want delete the point?"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("Close"),
                        onPressed: () => Navigator.of(context).pop(),
                      ),
                      FlatButton(
                        child: Text("Delete"),
                        onPressed: () {
                          _deleteMarker(markerWithKey);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                });
          },
        ),
      ),
    );
    markerWithKey.marker = marker;
    setState(() {
      Worker.mappingMemory[widget.addMarkersIndex].add(markerWithKey);
    });
  }

  void onMapLongPress(LatLng where) {
    if (Worker.mappingMemory[widget.stateIndex] != Consts.STATE_ADDING) {
      //смело может идти нахуй!
      return;
    }
    setState(() {
      if (Worker.mappingMemory[widget.droneMarkerIndex].width == -1) {
        Worker.mappingMemory[widget.droneMarkerIndex] = Marker(
          width: 60.0,
          height: 60.0,
          point: where,
          builder: (context) => new Container(
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text("Delete drone?"),
                        content:
                            Text("Are you sure you want delete the drone?"),
                        actions: <Widget>[
                          FlatButton(
                            child: Text("Close"),
                            onPressed: () => Navigator.of(context).pop(),
                          ),
                          FlatButton(
                            child: Text("Delete"),
                            onPressed: () {
                              setState(() {
                                Worker.mappingMemory[widget.droneMarkerIndex] =
                                    Marker(width: -1);
                              });
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    });
              },
              child: SvgPicture.asset(
                'assets/drone.svg',
                height: 60,
                width: 60,
              ),
            ),
          ),
        );
      } else {
        // пішов звідси, розбійник блядь!
        Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text('Drone has already been created!'),
          duration: new Duration(seconds: 3),
        ));
      }
    });
  }

  MapOptions _getMapOptions() {
    if (Worker.mappingMemory[widget.stateIndex] != Consts.STATE_MAKING_PHOTO) {
      return MapOptions(
        center: Consts.MAPPING_DEFAULT_LATLNG,
        onTap: onMapTap,
        zoom: widget.zoom,
        onLongPress: onMapLongPress,
      );
    } else
      return MapOptions(
        center: Worker.mappingMemory[widget.resultPhotoPointsIndex][0].dronePos,
        onTap: onMapTap,
        zoom: 18,
        onLongPress: onMapLongPress,
      );
  }

  List<Polyline> _getPolylines() {
    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_MAKING_PHOTO) {
      return [];
    }
    List<Polyline> res = List();
    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_NOTHING) {
      for (int i = 0;
          i < Worker.mappingMemory[widget.resultPointsIndex].length;
          i++) {
        res.add(Polyline(
          color: Colors.purple,
          strokeWidth: 5,
          points: [
            Worker.mappingMemory[widget.resultPointsIndex][i].marker.point,
            Worker
                .mappingMemory[widget.resultPointsIndex][(i + 1) %
                    Worker.mappingMemory[widget.resultPointsIndex].length]
                .marker
                .point,
          ],
          isDotted: true,
        ));
      }
      for (int i = 0;
          i < Worker.mappingMemory[widget.addMarkersIndex].length;
          i++) {
        res.add(Polyline(
          color: Colors.red,
          strokeWidth: 5,
          points: [
            Worker.mappingMemory[widget.addMarkersIndex][i].marker.point,
            Worker
                .mappingMemory[widget.addMarkersIndex][(i + 1) %
                    Worker.mappingMemory[widget.addMarkersIndex].length]
                .marker
                .point,
          ],
          isDotted: true,
        ));
      }

      for (int i = 0;
          i < Worker.mappingMemory[widget.resultPhotoPointsIndex].length;
          i++) {
        res.add(Polyline(
          color: Colors.green,
          strokeWidth: 3,
          points: [
            Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l1,
            Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l2,
            Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l3,
            Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l4,
            Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l1,
          ],
          isDotted: (i % 2 == 0),
        ));
      }
      return res;
    }

    for (int i = 0;
        i < Worker.mappingMemory[widget.addMarkersIndex].length;
        i++) {
      res.add(Polyline(
        color: Colors.red,
        strokeWidth: 5,
        points: [
          Worker.mappingMemory[widget.addMarkersIndex][i].marker.point,
          Worker
              .mappingMemory[widget.addMarkersIndex][
                  (i + 1) % Worker.mappingMemory[widget.addMarkersIndex].length]
              .marker
              .point,
        ],
        isDotted: true,
      ));
    }
    return res;
  }

  List<Marker> _getMarkers() {
    List<Marker> markers = List();
    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_MAKING_PHOTO) {
      int i = Worker.mappingScreenshotIndex;
      markers.add(
        Marker(
          width: 60.0,
          height: 60.0,
          point: Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l1,
          builder: (context) => Icon(
            Icons.brightness_1,
            color: Colors.pink,
          ),
        ),
      );
      markers.add(
        Marker(
          width: 60.0,
          height: 60.0,
          point: Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l2,
          builder: (context) => Icon(
            Icons.brightness_1,
            color: Colors.pink,
          ),
        ),
      );
      markers.add(
        Marker(
          width: 60.0,
          height: 60.0,
          point: Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l3,
          builder: (context) => Icon(
            Icons.brightness_1,
            color: Colors.pink,
          ),
        ),
      );
      markers.add(
        Marker(
          width: 60.0,
          height: 60.0,
          point: Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l4,
          builder: (context) => Icon(
            Icons.brightness_1,
            color: Colors.pink,
          ),
        ),
      );
      /*Polyline(
        color: Worker.isInDebugMode ? Colors.pink : Colors.white,
        strokeWidth: 2,
        points: [
          Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l1,
          Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l2,
          Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l3,
          Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l4,
          Worker.mappingMemory[widget.resultPhotoPointsIndex][i].l1,
        ],
        isDotted: false,
      )*/
      return markers;
    }

    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_NOTHING) {
      for (int i = 0;
          i < Worker.mappingMemory[widget.addMarkersIndex].length;
          i++) {
        markers.add(Worker.mappingMemory[widget.addMarkersIndex][i].marker);
      }
      for (int i = 0;
          i < Worker.mappingMemory[widget.resultPhotoPointsIndex].length;
          i++) {
        markers.add(Marker(
          width: 60.0,
          height: 60.0,
          point:
              Worker.mappingMemory[widget.resultPhotoPointsIndex][i].dronePos,
          builder: (context) => new Container(
            child: GestureDetector(
              onTap: () {
                if (widget.showPhotos) {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          content: Image.file(
                            File(Worker
                                .mappingMemory[widget.resultPhotoPointsIndex][i]
                                .path),
                          ),
                          contentPadding: EdgeInsets.all(0.0),
                        );
                      });
                }
              },
              child: Icon(
                Icons.brightness_1,
                color: Colors.green,
              ),
            ),
          ),
        ));
      }
      if (Worker.mappingShowDrone) {
        Marker _droneMarker = Marker(
          width: 60.0,
          height: 60.0,
          point: dronePos,
          builder: (context) => new Container(
            child: SvgPicture.asset(
              'assets/drone.svg',
              height: 60,
              width: 60,
            ),
          ),
        );
        markers.add(_droneMarker);
      }
      return markers;
    }
    for (int i = 0;
        i < Worker.mappingMemory[widget.addMarkersIndex].length;
        i++)
      markers.add(Worker.mappingMemory[widget.addMarkersIndex][i].marker);
    if (Worker.mappingMemory[widget.droneMarkerIndex].width != -1) {
      markers.add(Worker.mappingMemory[widget.droneMarkerIndex]);
    }
    return markers;
  }
}

class MapPage extends StatefulWidget {
  Session session;

  MapPage(this.session) {
    if (this.session.photos.length > 0) {
      double lat = this.session.photos[0].lat;
      double lng = this.session.photos[0].lng;
      Consts.MAPPING_DEFAULT_LATLNG = LatLng(lat, lng);
    }
  }

  @override
  State<StatefulWidget> createState() => _MapPageState();
  static final int stateIndex = 8;
  static final int droneMarkerIndex = 9;
  static final int addMarkersIndex = 10;
  static final int resultPointsIndex = 11;
  static final int resultPhotoPointsIndex = 12;
  static final int mapControllerIndex = 13;
  static final int dronePosControllerIndex = 14;
  static final int statisticsIndex = 15;
  static final stream = BehaviorSubject();

  static void updateDronePos() {
    double time = 0;
    Timer.periodic(Consts.DRONE_FRAME_DURATION, (timer) {
      if (!Worker.isDiagnosticsOpen ||
          Worker.mappingMemory[stateIndex] != Consts.STATE_NOTHING ||
          Worker.mappingMemory[resultPointsIndex].length <= 1) {
        timer.cancel();
      }
      //Worker.mappingMemory[dronePosControllerIndex].add(Worker.mappingMemory[resultPointsIndex][pos].marker.point);
      for (int i = 0; i < Worker.mappingMemory[resultPointsIndex].length; i++) {
        if (i == Worker.mappingMemory[resultPointsIndex].length - 1) {
          Worker.mappingMemory[dronePosControllerIndex]
              .add(Worker.mappingMemory[resultPointsIndex][i].marker.point);
        } else if (Worker.mappingMemory[resultPointsIndex][i + 1].time > time) {
          LatLng _l1 = Worker.mappingMemory[resultPointsIndex][i].marker.point;
          LatLng _l2 =
              Worker.mappingMemory[resultPointsIndex][(i + 1)].marker.point;
          double ratio =
              (time - Worker.mappingMemory[resultPointsIndex][i].time) /
                  (Worker.mappingMemory[resultPointsIndex][i + 1].time - time);
          LatLng _ll = LatLng(
              (_l1.latitude + ratio * _l2.latitude) / (ratio + 1),
              (_l1.longitude + ratio * _l2.longitude) / (ratio + 1));
          Worker.mappingMemory[dronePosControllerIndex].add(_ll);
          break;
        }
      }
      time += Worker.mappingAnimateSpeed *
          (Consts.DRONE_FRAME_DURATION.inMilliseconds / 1000.toDouble());
    });
  }
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    Consts.MAP_VALUE_ZOOM = 15;
    return StreamBuilder(
        stream: MapPage.stream,
        builder: (context, snapshot) => RouteMap(
              MapPage.stateIndex,
              MapPage.droneMarkerIndex,
              MapPage.addMarkersIndex,
              MapPage.resultPointsIndex,
              MapPage.resultPhotoPointsIndex,
              MapPage.mapControllerIndex,
              MapPage.dronePosControllerIndex,
              showPhotos: true,
            ));
  }
}
