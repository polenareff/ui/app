import 'dart:async' show Future;
import 'dart:convert' show json;
import 'dart:core';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:path/path.dart' as path;
import 'worker.dart';

class Secret {
  final String mapBoxKey;

  Secret({this.mapBoxKey = ""});

  factory Secret.fromJson(Map<String, dynamic> jsonMap) {
    return new Secret(mapBoxKey: jsonMap["mapbox_key"]);
  }
}

class SecretLoader {
  final String secretPath;

  SecretLoader({this.secretPath});

  Future<Secret> load() {
    return rootBundle.loadStructuredData<Secret>(this.secretPath,
        (jsonStr) async {
      final secret = Secret.fromJson(json.decode(jsonStr));
      return secret;
    });
  }
}

class Drone {
  int maxHeight = 0;
  int battery = 0;
  int batteryChangeKm = 0;
  int batteryChangePhoto = 0;
  int focusDistance = 0;
  String name = "";

  Drone(
      {this.name,
      this.maxHeight,
      this.battery,
      this.focusDistance,
      this.batteryChangeKm,
      this.batteryChangePhoto});

  factory Drone.fromJSON(String mapped) {
    Map<String, dynamic> map = json.decode(mapped);
    Drone res = Drone();
    res.maxHeight = map["max_height"];
    res.focusDistance = map["foc_len"];
    res.battery = map["battery"];
    res.batteryChangeKm = map["battery_change_km"];
    res.batteryChangePhoto = map["battery_change_photo"];
    res.name = map["name"];
    return res;
  }

  @override
  String toString() {
    Map<String, dynamic> map = Map();
    map["max_height"] = maxHeight;
    map["battery"] = battery;
    map["battery_change_km"] = batteryChangeKm;
    map["battery_change_photo"] = batteryChangePhoto;
    map["foc_len"] = focusDistance;
    map["name"] = name;
    return json.encode(map);
  }
}

class BackDropElementValue {
  int value;
  int minValue;
  int maxValue;
  int divisions;
  String key;

  BackDropElementValue(
      {defaultRuntimeValue,
      this.minValue,
      this.maxValue,
      this.divisions,
      this.key}) {
    this.value = getValue(defaultRuntimeValue);
  }

  factory BackDropElementValue.fromCheckBox(String key)
  {
    return BackDropElementValue(
      defaultRuntimeValue: 0,
      minValue: 0,
      maxValue: 1,
      divisions: 1,
      key: key,
    );
  }

  Future<bool> setValue(int value) {
    this.value = value;
    return Worker.prefs.setInt(key, value);
  }

  int getValue(int defaultValue) {
    return Worker.prefs.getInt(key) ?? defaultValue;
  }
}

class DronePhoto {
  LatLng l1, l2, l3, l4;
  LatLng dronePos;
  String path = "";
  DronePhoto(this.l1, this.l2, this.l3, this.l4, this.dronePos, {this.path});
}

class MarkerWithKey {
  UniqueKey key = UniqueKey();
  Marker marker;

  MarkerWithKey({this.marker});
}

class MarkerWithTime {
  Marker marker;
  double time = 0;

  MarkerWithTime({this.marker, this.time});
}

class HydratePoint {
  int hydrate = 0;
  int x = 0, y = 0;
  String message = "";
  HydratePoint({this.hydrate = 0, this.x = 0, this.y = 0, this.message=""});
}

class PhotoWithResult {
  int width = 0, height = 0;
  String path;
  List<List<HydratePoint>> hydratePoints = List();

  PhotoWithResult(this.path, this.hydratePoints, this.width, this.height);
}

class SessionPhoto {
  double lat = 0;
  double lng = 0;
  double alt = 0;
  double yaw = 0;
  String name="";
}

class Session {
  String workingDirectory="";
  String name;
  bool valid = false;
  List<SessionPhoto> photos;
  bool routing = false;
  Session();

  factory Session.fromJSON(Map<String, dynamic> contentJSON, String workingDirectory)
  {
    Session addSession = Session();
    bool valid = contentJSON['info']['telemetry'];
    bool routing = contentJSON['info']['fake'];
    addSession.name = path.basename(workingDirectory);
    addSession.valid = valid;
    addSession.workingDirectory = workingDirectory;
    addSession.photos = List();
    addSession.routing = routing;
    Directory viewPhotosDirectory = Directory(workingDirectory);
    Map<String, Map<String, dynamic>> photosMap = Map();
    for (var s in contentJSON['photos']) {
      photosMap[s['name']] = s;
    }
    for (FileSystemEntity photoEntity in viewPhotosDirectory.listSync()) {
      if (path.extension(photoEntity.path) == ".json" ||
          path.basenameWithoutExtension(photoEntity.path) == "thumbnail")
        continue;
      SessionPhoto sessionPhoto = SessionPhoto();
      sessionPhoto.name = path.basename(photoEntity.path);
      sessionPhoto.alt = photosMap[sessionPhoto.name]['alt'].toDouble();
      sessionPhoto.lat = photosMap[sessionPhoto.name]['lat'].toDouble();
      sessionPhoto.lng = photosMap[sessionPhoto.name]['lng'].toDouble();
      sessionPhoto.yaw = photosMap[sessionPhoto.name]['yaw'].toDouble();
      addSession.photos.add(sessionPhoto);
    }
    if (addSession.photos.length > 0) return addSession;
    return null;
  }
}

class UsefulFunctions {
  static Widget getAppTitle(String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        FractionallySizedBox(
          heightFactor: 0.6,
          child: Image.asset('assets/logo.png'),
        ),
        SizedBox(
          width: 10,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 3),
          child: Text(
            title,
            style: TextStyle(
              fontFamily: 'AlegreyaSansSC',
              fontSize: 27,
            ),
          ),
        ),
      ],
    );
    //Text("Полеnareff");
  }
}
