import 'dart:convert';
import 'dart:io';

import 'package:extended_image/extended_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:polenareff_app/image_viewer.dart';

import 'consts.dart';
import 'models.dart';
import 'worker.dart';

class HydrationPage extends StatefulWidget {
  HydrationPage(this.session, {Key key}) : super(key: key);
  Session session;
  static int addingState = Consts.STATE_NOTHING;
  static double imageScale = 1;

  @override
  _HydrationPageState createState() => _HydrationPageState();
}

class _HydrationPageState extends State<HydrationPage> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Worker.hydrationResultControllers[widget.session.name].stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Column(
            children: [
              Spacer(),
              Center(
                child: Text(
                  "Error happened!",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Center(
                child: Text(
                  snapshot.error.toString(),
                ),
              ),
              SizedBox(height: 20),
              RaisedButton(
                child: Text("Reload hydration request"),
                onPressed: () {
                  Worker.tryToMakeHydrationRequest(widget.session);
                },
              ),
              Spacer(),
            ],
          );
        } else if (snapshot.hasData) {
          List resultPhotos = snapshot.data[0];
          bool showProgress = snapshot.data[1];
          print(resultPhotos.length);
          return ExtendedImageGesturePageView.builder(
            itemBuilder: (BuildContext context, int index) {
              if (index==resultPhotos.length)
                return Center(child: CircularProgressIndicator(),);
              return PhotoMap(
                File(resultPhotos[index].path),
                resultPhotos[index].hydratePoints,
                resultPhotos[index].width,
                resultPhotos[index].height,
                true,
                showPercents: false,
              );
            },
            itemCount: resultPhotos.length + (showProgress ? 1 : 0),
            controller: PageController(
              initialPage: 0,
            ),
            scrollDirection: Axis.horizontal,
          );
        }
        return Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Spacer(flex: 1),
              CircularProgressIndicator(),
              Spacer(flex: 1),
            ],
          ),
        );
      },
    );
  }
}
