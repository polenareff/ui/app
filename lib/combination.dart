import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:path/path.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import 'consts.dart';
import 'models.dart';
import 'worker.dart';

class CombinationPage extends StatefulWidget {
  CombinationPage(this.session, {Key key}) : super(key: key);
  Session session;
  static int addingState = Consts.STATE_NOTHING;

  @override
  _CombinationPageState createState() => _CombinationPageState();
}

class _CombinationPageState extends State<CombinationPage> {
  int lastRightClickMs = -1000;
  PhotoViewController controller = PhotoViewController()..scale = 0.5;
  double minScale = 0.5;
  double maxScale = 10;
  double scaleChange = 0.2;

  void _pointerSignal(PointerSignalEvent details) {
    if (debugDefaultTargetPlatformOverride == TargetPlatform.fuchsia) {
      PointerScrollEvent event = details;
      if (event.scrollDelta.dy > 10) {
        double newScale = controller.scale - scaleChange;
        newScale = math.min(newScale, maxScale);
        newScale = math.max(newScale, minScale);
        controller.scale = newScale;
      } else if (event.scrollDelta.dy < -10) {
        double newScale = controller.scale + scaleChange;
        newScale = math.min(newScale, maxScale);
        newScale = math.max(newScale, minScale);
        controller.scale = newScale;
      }
      print(controller.scale);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Worker.combinationResultControllers[widget.session.name].stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Column(
            children: [
              Spacer(),
              Center(
                child: Text(
                  "Error happened!",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Center(
                child: Text(
                  snapshot.error.toString(),
                ),
              ),
              SizedBox(height: 20),
              RaisedButton(
                child: Text("Reload combination request"),
                onPressed: () {
                  Worker.tryToMakeCombinationRequest(widget.session, false);
                },
              ),
              Spacer(),
            ],
          );
        }
        if (snapshot.hasData) {
          scaleChange = 0.2;
          minScale = 0.5;
          maxScale = 10;
          controller.scale = 1.5;
          return Listener(
            onPointerSignal: _pointerSignal,
            child: PhotoView(
              imageProvider: Image.file(File(snapshot.data)).image,
              backgroundDecoration: BoxDecoration(color: Colors.white),
              minScale: minScale,
              maxScale: maxScale,
              controller: controller,
            ),
          );
        }
        return Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Spacer(flex: 1),
              CircularProgressIndicator(),
              Spacer(flex: 1),
            ],
          ),
        );
      },
    );
  }
}
