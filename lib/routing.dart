import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:http/http.dart' as http;
import 'package:latlong/latlong.dart' as lat_lng;
import 'package:latlong/latlong.dart';
import 'package:path/path.dart' as path;
import 'package:rxdart/rxdart.dart';
import 'package:xml/xml.dart' as xml;

import 'backdrop.dart';
import 'backdrop_content.dart';
import 'choose_session.dart';
import 'consts.dart';
import 'mapping.dart';
import 'messenger.dart';
import 'models.dart';
import 'worker.dart';

class RoutingPage extends StatefulWidget {
  RoutingPage({Key key}) : super(key: key);

  @override
  _RoutingPageState createState() => _RoutingPageState();

  var scaffoldKey = Worker.routingScaffoldKey;
  final int stateIndex = 0;
  final int droneMarkerIndex = 1;
  final int addMarkersIndex = 2;
  final int resultPointsIndex = 3;
  final int resultPhotoPointsIndex = 4;
  final int mapControllerIndex = 5;
  final int dronePosControllerIndex = 6;
  final int statisticsIndex = 7;
  String folderName = "";
  final photoStateController = BehaviorSubject();

  void tryToMakeRoutingRequest(Function onCancel, Function onReady,
      Function onFinalError, GlobalKey<ScaffoldState> scaffoldKey) {
    if (Worker.mappingMemory[droneMarkerIndex].width == -1) {
      Messenger.makeMessage("You haven't pointed the drone!", scaffoldKey);
      onCancel();
      return;
    }
    if (Worker.mappingMemory[addMarkersIndex].length == 0) {
      Messenger.makeMessage("You haven't made any points!", scaffoldKey);
      onCancel();
      return;
    }
    Function onError = (dynamic response) {
      Messenger.makeMessage(Consts.MESSAGE_ROUTING_ERROR, scaffoldKey,
          duration: Duration(seconds: 10),
          buttonMessage: "Try again", onClosed: (SnackBarClosedReason reason) {
        if (reason != SnackBarClosedReason.remove &&
            reason != SnackBarClosedReason.action) {
          onFinalError();
        }
      }, onButtonPressed: () async {
        await Future.delayed(Duration(milliseconds: 100));
        tryToMakeRoutingRequest(onCancel, onReady, onFinalError, scaffoldKey);
      });
    };
    Function onLoading = () {
      Messenger.makeMessage(
        Consts.MESSAGE_ROUTING_PROCESSING,
        scaffoldKey,
        duration: Duration(minutes: 5),
        constant: true,
      );
    };
    Messenger.makeMessage(
      Consts.MESSAGE_ROUTING_FUTURE_REQUEST,
      scaffoldKey,
      duration: Duration(milliseconds: 2500),
      buttonMessage: "Undo",
      onButtonPressed: () {
        onCancel();
      },
      onClosed: (SnackBarClosedReason reason) {
        if (reason != SnackBarClosedReason.remove &&
            reason != SnackBarClosedReason.action) {
          Map<String, dynamic> droneParams = Worker.getDroneParams();
          droneParams['x'] =
              Worker.mappingMemory[droneMarkerIndex].point.longitude;
          droneParams['y'] =
              Worker.mappingMemory[droneMarkerIndex].point.latitude;
          Map<String, dynamic> params = Map();
          params['drone'] = droneParams;
          params['field'] = List();
          for (int i = 0;
              i < Worker.mappingMemory[addMarkersIndex].length;
              i++) {
            Map<String, dynamic> xy = Map();
            xy['x'] =
                Worker.mappingMemory[addMarkersIndex][i].marker.point.longitude;
            xy['y'] =
                Worker.mappingMemory[addMarkersIndex][i].marker.point.latitude;
            params['field'].add(xy);
          }
          Worker.fetchRouting(json.encode(params),
              onReady: onReady, onError: onError, onLoading: onLoading);
        }
      },
    );
  }

  void updateDronePos() {
    double time = 0;
    Timer.periodic(Consts.DRONE_FRAME_DURATION, (timer) {
      if (Worker.mappingMemory[stateIndex] != Consts.STATE_NOTHING ||
          Worker.mappingMemory[resultPointsIndex].length <= 1) {
        timer.cancel();
      }
      for (int i = 0; i < Worker.mappingMemory[resultPointsIndex].length; i++) {
        if (i == Worker.mappingMemory[resultPointsIndex].length - 1) {
          Worker.mappingMemory[dronePosControllerIndex]
              .add(Worker.mappingMemory[resultPointsIndex][i].marker.point);
        } else if (Worker.mappingMemory[resultPointsIndex][i + 1].time > time) {
          lat_lng.LatLng _l1 =
              Worker.mappingMemory[resultPointsIndex][i].marker.point;
          lat_lng.LatLng _l2 =
              Worker.mappingMemory[resultPointsIndex][(i + 1)].marker.point;
          double ratio =
              (time - Worker.mappingMemory[resultPointsIndex][i].time) /
                  (Worker.mappingMemory[resultPointsIndex][i + 1].time - time);
          lat_lng.LatLng _ll = lat_lng.LatLng(
              (_l1.latitude + ratio * _l2.latitude) / (ratio + 1),
              (_l1.longitude + ratio * _l2.longitude) / (ratio + 1));
          Worker.mappingMemory[dronePosControllerIndex].add(_ll);
          break;
        }
      }
      time += Consts.DRONE_FRAME_DURATION.inMilliseconds / 1000.toDouble();
    });
  }

  void mapZoomIn({double customZoom = 0.1, bool bs = true}) {
    if (bs) {
      _mapZoomIn(customZoom);
      _mapZoomOut(customZoom);
      _mapZoomIn(customZoom);
      //Пошли нахуй, пишу код как хочу
    } else {
      _mapZoomIn(customZoom);
    }
  }

  void mapZoomOut({double customZoom = 0.1, bool bs = true}) {
    _mapZoomOut(customZoom);
  }

  void _mapZoomIn(double changeZoom) {
    double zoom = Worker.mappingMemory[mapControllerIndex].zoom;
    zoom += changeZoom;
    zoom = min(zoom, Consts.MAP_MAX_ZOOM);
    zoom = max(zoom, Consts.MAP_MIN_ZOOM);
    Worker.mappingMemory[mapControllerIndex]
        .move(Worker.mappingMemory[mapControllerIndex].center, zoom);
    //routingZoom = Worker.mappingMemory[mapControllerIndex].zoom;
  }

  void _mapZoomOut(double changeZoom) {
    double zoom = Worker.mappingMemory[mapControllerIndex].zoom;
    zoom -= changeZoom;
    zoom = min(zoom, Consts.MAP_MAX_ZOOM);
    zoom = max(zoom, Consts.MAP_MIN_ZOOM);
    Worker.mappingMemory[mapControllerIndex]
        .move(Worker.mappingMemory[mapControllerIndex].center, zoom);
    //routingZoom = Worker.mappingMemory[mapControllerIndex].zoom;
  }

  double getAngle(double long1, double lat1, double long2, double lat2) {
    long1 = lat_lng.degToRadian(long1);
    long2 = lat_lng.degToRadian(long2);
    lat1 = lat_lng.degToRadian(lat1);
    lat2 = lat_lng.degToRadian(lat2);

    double dLon = (long2 - long1);

    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);

    double brng = atan2(y, x);

    brng = lat_lng.radianToDeg(brng);
    brng = (brng + 360) % 360;
    //brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

    return brng;
  }

  void saveDroneInstructions() {
    Map<String, dynamic> finalJSON = Map();
    File file = File(path.join(Worker.writeResultsPath, "instructions.json"));
    List<Map<String, dynamic>> photoJSON = List();
    for (int i = 0;
        i < Worker.mappingMemory[resultPhotoPointsIndex].length;
        i++) {
      double x2 =
              (Worker.mappingMemory[resultPhotoPointsIndex][i].l2.longitude +
                      Worker.mappingMemory[resultPhotoPointsIndex][i].l3
                          .longitude) /
                  2,
          y2 = (Worker.mappingMemory[resultPhotoPointsIndex][i].l2.latitude +
                  Worker.mappingMemory[resultPhotoPointsIndex][i].l3.latitude) /
              2;
      double angle = getAngle(
          Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.longitude,
          Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.latitude,
          x2,
          y2);
      photoJSON.add({
        'lat':
            Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.latitude,
        'lng':
            Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.longitude,
        'alt': Worker.mappingMemory[statisticsIndex]['optimal_height'],
        'yaw': 180 + angle,
        'roll': 0,
        'pitch': 0,
        'x1': Worker.mappingMemory[resultPhotoPointsIndex][i].l1.longitude,
        'y1': Worker.mappingMemory[resultPhotoPointsIndex][i].l1.latitude,
        'x2': Worker.mappingMemory[resultPhotoPointsIndex][i].l2.longitude,
        'y2': Worker.mappingMemory[resultPhotoPointsIndex][i].l2.latitude,
        'x3': Worker.mappingMemory[resultPhotoPointsIndex][i].l3.longitude,
        'y3': Worker.mappingMemory[resultPhotoPointsIndex][i].l3.latitude,
        'x4': Worker.mappingMemory[resultPhotoPointsIndex][i].l4.longitude,
        'y4': Worker.mappingMemory[resultPhotoPointsIndex][i].l4.latitude,
      });
    }
    finalJSON['drone'] = Worker.getDroneParams();
    finalJSON['photos'] = photoJSON;
    file.writeAsString(json.encode(finalJSON)).then((value) {
      Messenger.makeMessage(
          "Instructions saved in instructions.json", scaffoldKey);
    });
  }

  void startCreatingSession(context) {
    Worker.mappingMemory[stateIndex] = Consts.STATE_MAKING_PHOTO;
    Worker.mappingScreenshotIndex = 0;
    Messenger.makeMessage(Consts.MESSAGE_MAKING_DRONE_PHOTOS, scaffoldKey,
        duration: Duration(seconds: 1000));
    List<Map<String, dynamic>> photoJSON = List();
    Map<String, dynamic> droneJSON = Worker.getDroneParams();
    photoStateController.add(0);
    int i = 0;
    Map<String, dynamic> infoJSON = Map();
    infoJSON['telemetry'] = true;
    infoJSON['fake'] = true;
    File file = File(path.join(Worker.writeResultsPath, "sessions"));
    if (!file.existsSync()) {
      Directory(file.path).createSync();
    }
    String folderPath = path.join(
        Worker.writeResultsPath, path.join("sessions", "$folderName"));
    Directory(folderPath).createSync();
    Timer.periodic(Duration(milliseconds: 2500), (timer) {
      if (Worker.mappingMemory[stateIndex] != Consts.STATE_MAKING_PHOTO) {
        timer.cancel();
        return;
      }
      if (i >= Worker.mappingMemory[resultPhotoPointsIndex].length) {
        Map<String, dynamic> finalJSON = Map();
        File file = File(path.join(folderPath, "meta.json"));
        finalJSON['drone'] = droneJSON;
        finalJSON['photos'] = photoJSON;
        finalJSON['info'] = infoJSON;
        file.writeAsString(json.encode(finalJSON));
        timer.cancel();
        Messenger.makeMessage(
          Consts.MESSAGE_MADE_DRONE_PHOTOS,
          scaffoldKey,
          duration: Duration(seconds: 1000),
          buttonMessage: "Open diagnostics",
          onButtonPressed: () => ChooseSession.openDiagnostics(
              Session.fromJSON(finalJSON, folderPath), context,
              replace: false),
        );
        stopCreatingSession();
        return;
      }
      try {
        String newName = "f$i.png";
        int newIName = i;
        Future.delayed(Duration(milliseconds: 2300), () async {
          Worker.mappingScreenShotController
              .capture(path: path.join(folderPath, newName))
              .then((File image) {
            var bytes = image.readAsBytesSync();
            compute(Worker.cropRoutingImage, bytes).then((list) {
              image.writeAsBytes(list[0]);
              if (newIName == 0) {
                File thumbnailFile =
                    File(path.join(folderPath, "thumbnail.jpg"));
                thumbnailFile.createSync();
                thumbnailFile.writeAsBytesSync(list[1]);
              }
            });
          });
        });
      } catch (e) {}
      double x2 =
              (Worker.mappingMemory[resultPhotoPointsIndex][i].l2.longitude +
                      Worker.mappingMemory[resultPhotoPointsIndex][i].l3
                          .longitude) /
                  2,
          y2 = (Worker.mappingMemory[resultPhotoPointsIndex][i].l2.latitude +
                  Worker.mappingMemory[resultPhotoPointsIndex][i].l3.latitude) /
              2;
      double angle = getAngle(
          Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.longitude,
          Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.latitude,
          x2,
          y2);
      Worker.mappingMemory[mapControllerIndex].rotate(180 - angle);
      photoJSON.add({
        'lat':
            Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.latitude,
        'lng':
            Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos.longitude,
        'alt': Worker.mappingMemory[statisticsIndex]['optimal_height'],
        'yaw': 180 + angle,
        'roll': 0,
        'pitch': 0,
        'x1': Worker.mappingMemory[resultPhotoPointsIndex][i].l1.longitude,
        'y1': Worker.mappingMemory[resultPhotoPointsIndex][i].l1.latitude,
        'x2': Worker.mappingMemory[resultPhotoPointsIndex][i].l2.longitude,
        'y2': Worker.mappingMemory[resultPhotoPointsIndex][i].l2.latitude,
        'x3': Worker.mappingMemory[resultPhotoPointsIndex][i].l3.longitude,
        'y3': Worker.mappingMemory[resultPhotoPointsIndex][i].l3.latitude,
        'x4': Worker.mappingMemory[resultPhotoPointsIndex][i].l4.longitude,
        'y4': Worker.mappingMemory[resultPhotoPointsIndex][i].l4.latitude,
        'name': "f$i.png",
      });
      if (i < Worker.mappingMemory[resultPhotoPointsIndex].length) {
        photoStateController.add(i);
        Worker.mappingMemory[mapControllerIndex].move(
            Worker.mappingMemory[resultPhotoPointsIndex][i].dronePos, 13.0);
        Worker.mappingMemory[mapControllerIndex].fitBounds(LatLngBounds(
            Worker.mappingMemory[resultPhotoPointsIndex][i].l2,
            Worker.mappingMemory[resultPhotoPointsIndex][i].l4));
        double photoW = droneJSON['photo_w'];
        mapZoomOut(customZoom: 0.5);
        /*double a = (1.5-MediaQuery.of(context).size.width/MediaQuery.of(context).size.height);
        if (a>=0)
          mapZoomOut(customZoom: 0.6);*/
      }
      i++;
    });
  }

  void stopCreatingSession() {
    Worker.mappingMemory[stateIndex] = Consts.STATE_NOTHING;
    photoStateController.add(0);
  }
}

class _RoutingPageState extends State<RoutingPage> {
  Widget getStatisticsWidget(Map<String, dynamic> statistics) {
    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_NOTHING &&
        statistics != null) {
      print(statistics);
      return IconButton(
        icon: Icon(Icons.info),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return DefaultTextStyle(
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontFamily: 'Roboto',
                    fontFamilyFallback: ['Roboto']),
                child: AlertDialog(
                  title: Text('Statistics'),
                  content: DataTable(
                    rows: [
                      DataRow(cells: [
                        DataCell(Text("Optimal height (m)")),
                        DataCell(Text("${statistics['optimal_height']}")),
                      ]),
                      DataRow(cells: [
                        DataCell(Text("Flew (km)")),
                        DataCell(Text("${statistics['km_flew'].round()}")),
                      ]),
                      DataRow(cells: [
                        DataCell(Text("Minimum battery level (units)")),
                        DataCell(Text("${statistics['battery_min'].round()}")),
                      ]),
                      DataRow(cells: [
                        DataCell(Text("Minimum battery packs")),
                        DataCell(
                            Text("${statistics['battery_packs'].round()}")),
                      ]),
                    ],
                    columns: [
                      DataColumn(label: Text("Name")),
                      DataColumn(label: Text("Data")),
                    ],
                  ),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text('Close'),
                    ),
                  ],
                ),
              );
            },
          );
        },
        tooltip: "Statistics",
      );
    }
    return SizedBox();
  }

  void _showSaveDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(title: Text("What to do next?"), children: [
            SimpleDialogOption(
              child: Text("Save drone instructions"),
              onPressed: () {
                widget.saveDroneInstructions();
                Navigator.pop(context);
              },
            ),
            SimpleDialogOption(
              child: Text("Create a new session"),
              onPressed: () {
                Navigator.pop(context);
                _showNameDialog();
              },
            ),
          ]);
        });
  }

  void _showNameDialog() {
    TextEditingController _textFieldController = TextEditingController();
    showDialog(
      context: context,
      builder: (context1) {
        return AlertDialog(
          title: Text("Choose session name"),
          content: TextField(
            controller: _textFieldController,
            decoration: InputDecoration(hintText: "Enter name here"),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("Create"),
              onPressed: () {
                widget.folderName = _textFieldController.text.toString();
                Navigator.of(context).pop();
                widget.startCreatingSession(context);
              },
            ),
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  void _onRoutingFABPressed() {
    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_NOTHING) {
      _showSaveDialog();
    } else if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_ADDING) {
      setState(() {
        Worker.mappingMemory[widget.stateIndex] = Consts.STATE_PROCESSING;
      });
      Function f = () => setState(() {
            Worker.mappingMemory[widget.stateIndex] = Consts.STATE_ADDING;
          });

      Function onReady = (http.Response response) {
        var decodedJSON = jsonDecode(response.body);
        var pointsJSON = decodedJSON['points'];
        var statsJSON = decodedJSON['stats'];
        Worker.mappingMemory[widget.statisticsIndex] = statsJSON;
        Worker.mappingMemory[widget.resultPointsIndex].clear();
        Worker.mappingMemory[widget.resultPhotoPointsIndex].clear();
        double time = 0;
        bool flag = false;
        lat_lng.LatLng prevPoint = lat_lng.LatLng(0, 0);
        for (dynamic json in pointsJSON) {
          Marker marker = Marker(
            width: 45.0,
            height: 45.0,
            anchorPos: AnchorPos.align(AnchorAlign.top),
            point: lat_lng.LatLng(json['y'], json['x']),
            builder: (context) => Container(
              child: Icon(
                Icons.location_on,
                color: Colors.blue,
                size: 45,
              ),
            ),
          );
          if (flag) {
            time +=
                Worker.distLatLng(marker.point, prevPoint) / Consts.DRONE_SPEED;
          } else
            flag = true;
          Worker.mappingMemory[widget.resultPointsIndex]
              .add(MarkerWithTime(marker: marker, time: time));
          if (json['photo']) {
            time += 2;
            Worker.mappingMemory[widget.resultPointsIndex]
                .add(MarkerWithTime(marker: marker, time: time));
            lat_lng.LatLng l1, l2, l3, l4;
            l1 = lat_lng.LatLng(json['y1'], json['x1']);
            l2 = lat_lng.LatLng(json['y2'], json['x2']);
            l3 = lat_lng.LatLng(json['y3'], json['x3']);
            l4 = lat_lng.LatLng(json['y4'], json['x4']);
            Worker.mappingMemory[widget.resultPhotoPointsIndex]
                .add(DronePhoto(l1, l2, l3, l4, marker.point));
          }
          prevPoint = marker.point;
        }
        Messenger.makeMessage(Consts.MESSAGE_ROUTING_OK, widget.scaffoldKey);
        setState(() {
          Worker.mappingMemory[widget.stateIndex] = Consts.STATE_NOTHING;
        });
        widget.updateDronePos();
      };
      Function onFinalError = () {
        setState(() {
          Worker.mappingMemory[widget.stateIndex] = Consts.STATE_ADDING;
        });
      };
      widget.tryToMakeRoutingRequest(
        f,
        onReady,
        onFinalError,
        widget.scaffoldKey,
      );
    }
  }

  Widget _getFAB() {
    if (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_MAKING_PHOTO ||
        Worker.mappingMemory[widget.stateIndex] == Consts.STATE_PROCESSING) {
      return null;
    }
    return (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_NOTHING)
        ? FloatingActionButton(
            key: UniqueKey(),
            onPressed: () => _onRoutingFABPressed(),
            child: Icon(Icons.save),
            backgroundColor: Consts.COLOR_MAIN,
            tooltip: "Save route",
          )
        : FloatingActionButton(
            key: UniqueKey(),
            onPressed: () => _onRoutingFABPressed(),
            child: Icon(Icons.done),
            backgroundColor: Consts.COLOR_MAIN,
            tooltip: "Calculate route",
          );
  }

  @override
  void didUpdateWidget(RoutingPage oldWidget) {
    setState(() {});
    super.didUpdateWidget(oldWidget);
  }

  void uploadGPX() async {
    var file = await FilePicker.getFile(type: FileType.ANY);
    setState(() {
      Worker.mappingMemory[widget.addMarkersIndex].clear();
      Worker.routingCanCreateMarker = false;
    });
    String kmlString = file.readAsStringSync();
    var document = xml.parse(kmlString);
    List<xml.XmlElement> coordinateList =
        document.findAllElements("coordinates").toList();
    print(coordinateList);
    for (xml.XmlElement element in coordinateList)
      {
        String s = element.toString();
        s = s.substring(13, s.length-14);
        List<String> arr = s.split(',');
        LatLng latLng = LatLng(double.parse(arr[1]), double.parse(arr[0]));
        MarkerWithKey markerWithKey = MarkerWithKey();
        print(latLng);
        Marker marker = Marker(
          width: 45.0,
          height: 45.0,
          anchorPos: AnchorPos.align(AnchorAlign.top),
          point: latLng,
          builder: (context) => new Container(
            child: Icon(
                Icons.location_on,
                color: Colors.blue,
                size: 45,
              ),
          ),
        );
        markerWithKey.marker = marker;
        Worker.mappingMemory[widget.addMarkersIndex].add(markerWithKey);
      }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.photoStateController.stream,
      builder: (context, snapshot) {
        Worker.mappingScreenshotIndex = snapshot.data;
        return BackdropScaffold(
          backLayer: BackdropContent(),
          title: UsefulFunctions.getAppTitle("Полеnareff"),
          headerHeight: MediaQuery.of(context).size.height / 5,
          headerColor: Colors.white.withOpacity(0.6),
          leadingWidget: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
            tooltip: "Go back, in menu",
          ),
          actions: <Widget>[
            getStatisticsWidget(Worker.mappingMemory[widget.statisticsIndex]),
            (Worker.mappingMemory[widget.stateIndex] == Consts.STATE_ADDING)
                ? IconButton(
                    icon: Icon(Icons.file_upload),
                    tooltip: "Upload KML file",
                    onPressed: () => uploadGPX(),
                  )
                : SizedBox(
                    width: 0,
                  ),
          ],
          frontLayer: SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: Scaffold(
              key: widget.scaffoldKey,
              body: RouteMap(
                  widget.stateIndex,
                  widget.droneMarkerIndex,
                  widget.addMarkersIndex,
                  widget.resultPointsIndex,
                  widget.resultPhotoPointsIndex,
                  widget.mapControllerIndex,
                  widget.dronePosControllerIndex),
              floatingActionButton: AnimatedSwitcher(
                  duration: Duration(milliseconds: 200), child: _getFAB()),
            ),
          ),
        );
      },
    );
  }
}
