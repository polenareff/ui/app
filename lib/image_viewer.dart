import 'dart:io';
import 'dart:math' as math;
import 'dart:math';
import 'dart:ui' as ui;
import 'package:extended_image/extended_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:polenareff_app/models.dart';
import 'package:polenareff_app/worker.dart';

// ignore: must_be_immutable
class PhotoMap extends StatefulWidget {
  final File file;
  final List<List<HydratePoint>> list;
  double zoom = 0.9;
  double minScale = 0.9;
  double maxScale = 3;
  int photoWidth = 0, photoHeight = 0;
  bool isInPageView = true;
  bool showPercents = true;
  bool showMessages = false;
  bool showIndicator = false;

  PhotoMap(this.file, this.list, this.photoWidth, this.photoHeight,
      this.isInPageView,
      {this.minScale = 0.9,
      this.maxScale = 3,
      this.zoom = 0.9,
      this.showPercents = true,
      this.showMessages = false,
      this.showIndicator = false});

  @override
  State<StatefulWidget> createState() => _PhotoMapState();
}

class _PhotoMapState extends State<PhotoMap> {
  int lastRightClickMs = -1000;

  void _pointerSignal(PointerSignalEvent details) {
    if (debugDefaultTargetPlatformOverride == TargetPlatform.fuchsia) {
      PointerScrollEvent event = details;
      if (event.scrollDelta.dy > 10) {
        setState(() {
          widget.zoom -= 0.25;
          widget.zoom = math.max(widget.minScale, widget.zoom);
          widget.zoom = math.min(widget.maxScale, widget.zoom);
        });
      } else if (event.scrollDelta.dy < -10) {
        setState(() {
          widget.zoom += 0.25;
          widget.zoom = math.max(widget.minScale, widget.zoom);
          widget.zoom = math.min(widget.maxScale, widget.zoom);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerSignal: _pointerSignal,
      child: ExtendedImage.file(
        widget.file,
        mode: ExtendedImageMode.gesture,
        fit: BoxFit.contain,
        initGestureConfigHandler: (state) {
          return GestureConfig(
            minScale: widget.minScale,
            animationMinScale: min(0.7, widget.minScale),
            maxScale: widget.maxScale,
            initialScale: widget.zoom,
            animationMaxScale: max(3.5, widget.maxScale),
            speed: 1.0,
            inertialSpeed: 100.0,
            inPageView: widget.isInPageView,
          );
        },
        scale: widget.zoom,
        afterPaintImage:
            (Canvas canvas, Rect rect, ui.Image image, Paint paint) {
          for (List<HydratePoint> cur in widget.list) {
            List<Offset> offsetList = List();
            for (HydratePoint point in cur) {
              double ratioX = 0, ratioY = 0;
              ratioX = point.x / (widget.photoWidth - point.x + 1);
              ratioY = point.y / (widget.photoHeight - point.y + 1);

              offsetList.add(Offset(
                (rect.left + rect.right * ratioX) / (ratioX + 1),
                (rect.top + rect.bottom * ratioY) / (ratioY + 1),
              ));
            }

            for (int i = 0; i < offsetList.length; i++) {
              canvas.drawLine(
                  offsetList[i],
                  offsetList[(i + 1) % offsetList.length],
                  Paint()
                    ..color = Worker.getHydrationColor(
                        cur[0].hydrate, widget.showMessages)
                    ..strokeWidth = 3);
            }
            if (widget.showPercents) {
              final builder =
                  ui.ParagraphBuilder(ui.ParagraphStyle(fontSize: 20))
                    ..addText("${cur[0].hydrate} %");
              final paragraph = builder.build();
              paragraph.layout(ui.ParagraphConstraints(width: double.infinity));
              canvas.drawParagraph(paragraph, offsetList[0]);
            }
            if (widget.showMessages) {
              final builder =
                  ui.ParagraphBuilder(ui.ParagraphStyle(fontSize: 18))
                    ..addText("${cur[0].message}");
              final paragraph = builder.build();
              paragraph.layout(ui.ParagraphConstraints(width: double.infinity));
              canvas.drawParagraph(paragraph, offsetList[0]);
            }
          }
        },
        onDoubleTap: (state) {},
      ),
    );
  }
}
