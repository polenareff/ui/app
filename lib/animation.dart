import 'dart:io';
import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view.dart';

import 'consts.dart';
import 'models.dart';
import 'worker.dart';

class AnimationPage extends StatefulWidget {
  AnimationPage(this.session, {Key key}) : super(key: key);
  Session session;
  static int addingState = Consts.STATE_NOTHING;

  @override
  _AnimationPageState createState() => _AnimationPageState();
}

class _AnimationPageState extends State<AnimationPage> {
  int lastRightClickMs = -1000;
  PhotoViewController controller = PhotoViewController()..scale = 1;
  double minScale = 1;
  double maxScale = 10;
  double scaleChange = 0.2;

  void _pointerSignal(PointerSignalEvent details) {
    if (debugDefaultTargetPlatformOverride == TargetPlatform.fuchsia) {
      PointerScrollEvent event = details;
      if (event.scrollDelta.dy > 10) {
        double newScale = controller.scale - scaleChange;
        newScale = math.min(newScale, maxScale);
        newScale = math.max(newScale, minScale);
        controller.scale = newScale;
      } else if (event.scrollDelta.dy < -10) {
        double newScale = controller.scale + scaleChange;
        newScale = math.min(newScale, maxScale);
        newScale = math.max(newScale, minScale);
        controller.scale = newScale;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Worker.animationResultControllers[widget.session.name].stream,
      builder: (context, snapshot)
      {
        if (snapshot.hasError)
        {
          return Column(
            children: [
              Spacer(),
              Center(
                child: Text(
                  "Error happened!",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Center(
                child: Text(
                  snapshot.error.toString(),
                ),
              ),
              SizedBox(height: 20),
              RaisedButton(
                child: Text("Reload animation request"),
                onPressed: () {
                  Worker.tryToMakeCombinationRequest(widget.session, true);
                },
              ),
              Spacer(),
            ],
          );
        }
        if (snapshot.hasData)
        {
          scaleChange = 0.2;
          minScale = 1;
          maxScale = 10;
          controller.scale = minScale;
          return Listener(
            onPointerSignal: _pointerSignal,
            child: PhotoView(
              imageProvider: Image.file(File(snapshot.data)).image,
              backgroundDecoration: BoxDecoration(color: Colors.white),
              minScale: minScale,
              maxScale: maxScale,
              controller: controller,
            ),
          );
        }
        return Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Spacer(flex: 1),
              CircularProgressIndicator(),
              Spacer(flex: 1),
            ],
          ),
        );
      },
    );
  }
}
