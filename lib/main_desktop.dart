import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'root.dart';
import 'worker.dart';

Future main() async {
	WidgetsFlutterBinding.ensureInitialized();
  Worker.rewriteDebug = "debug";
  Worker.prefs = await SharedPreferences.getInstance();
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  Directory dir = await getApplicationDocumentsDirectory();
  Worker.writeResultsPath = dir.path;
  print(Worker.writeResultsPath);
  runApp(PoleApp());
}
