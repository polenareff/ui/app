import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:http/http.dart' as http;
import 'package:image/image.dart' as imglib;
import 'package:latlong/latlong.dart' as lat_lng;
import 'package:latlong/latlong.dart';
import 'package:path/path.dart';
import 'package:path/path.dart' as path;
import 'package:rxdart/rxdart.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'consts.dart';
import 'mapping.dart';
import 'models.dart';

class Worker {
  static String writeResultsPath = "";
  static SharedPreferences prefs;
  static GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  static var rng = Random.secure();
  static int copyMemoryIndex = 0;
  static List mappingMemory = [
    Consts.STATE_ADDING,
    Marker(width: -1),
    List(),
    List(),
    List(),
    MapController(),
    StreamController<LatLng>.broadcast(),
    Map<String, dynamic>(),
    Consts.STATE_ADDING,
    Marker(width: -1),
    List(),
    List(),
    List(),
    MapController(),
    StreamController<LatLng>.broadcast(),
    Map<String, dynamic>(),
  ];

  static BackDropElementValue droneFocusDistance = BackDropElementValue(
          defaultRuntimeValue: 30,
          minValue: 4,
          maxValue: 60,
          divisions: 28,
          key: "drone_foc_len"),
      droneMaxHeight = BackDropElementValue(
          defaultRuntimeValue: 300,
          minValue: 50,
          maxValue: 500,
          divisions: 45,
          key: "drone_max_height"),
      droneBattery = BackDropElementValue(
          defaultRuntimeValue: 10000,
          minValue: 1000,
          maxValue: 20000,
          divisions: 190,
          key: "drone_battery"),
      droneBatteryChangeKm = BackDropElementValue(
          defaultRuntimeValue: 250,
          minValue: 250,
          maxValue: 500,
          divisions: 25,
          key: "drone_battery_change_km"),
      droneBatteryChangePhoto = BackDropElementValue(
          defaultRuntimeValue: 30,
          minValue: 20,
          maxValue: 200,
          divisions: 18,
          key: "drone_battery_change_photo"),
      dronePhotoOverlay = BackDropElementValue(
          defaultRuntimeValue: 0,
          minValue: 0,
          maxValue: 30,
          divisions: 15,
          key: "drone_photo_overlay"),
      dronePacksAmount = BackDropElementValue(
          defaultRuntimeValue: 1,
          minValue: 1,
          maxValue: 10,
          divisions: 9,
          key: "drone_packs_amount"),
      droneManyPacks = BackDropElementValue.fromCheckBox("drone_many_packs");
  static List<Uint8List> importList = List();
  static List<File> importFileList = List();
  static Session comparingSession;
  static File importMetaFile;
  static int importCurrentIndex = 0;
  static bool isImportCompressingPhotos = false;
  static bool isImportNetworking = false;
  static bool isDiagnosticsOpen = false;
  static double mappingAnimateSpeed = 1;
  static int mappingScreenshotIndex = 0;
  static bool mappingShowDrone = true;
  static bool routingCanCreateMarker = true;
  static final ScreenshotController mappingScreenShotController =
      ScreenshotController();

  static TextEditingController droneNameController =
      TextEditingController(text: prefs.getString(Consts.PREFS_DRONE_NAME));

  static String rewriteDebug = "";
  static var routingScaffoldKey = GlobalKey<ScaffoldState>();
  static final Map<String, BehaviorSubject> combinationResultControllers =
      Map();
  static final Map<String, BehaviorSubject> hydrationResultControllers = Map();
  static final Map<String, BehaviorSubject> animationResultControllers = Map();
  static BehaviorSubject comparisonController = BehaviorSubject();
  static List tempHydrationList = [];
  static List tempComparisonList = [];

  static void makeSessionRequests(Session chosenSession) async {
    List res = Worker.parseRouting(chosenSession.workingDirectory,
        path.join(chosenSession.workingDirectory, "meta.json"));
    Worker.mappingMemory[MapPage.resultPointsIndex] = res[0];
    Worker.mappingMemory[MapPage.resultPhotoPointsIndex] = res[1];
    Worker.mappingMemory[MapPage.stateIndex] = Consts.STATE_NOTHING;
    Worker.mappingMemory[MapPage.droneMarkerIndex] = res[0][0].marker;
    int a = 1;
    MapPage.updateDronePos();
    MapPage.stream.add(a);
    Future.delayed(Duration(seconds: 1), () async {
      Worker.tryToMakeHydrationRequest(chosenSession);
      if (chosenSession.valid) {
        Worker.tryToMakeCombinationRequest(chosenSession, false);
        Worker.tryToMakeCombinationRequest(chosenSession, true);
      }
    });
  }

  static double distLatLng(LatLng ll1, LatLng ll2) {
    double lat1 = ll1.latitudeInRad,
        lng1 = ll1.longitudeInRad,
        lat2 = ll2.latitudeInRad,
        lng2 = ll2.longitudeInRad;
    double earthRadius = 6371000; //meters
    double dLat = lat2 - lat1;
    double dLng = lng2 - lng1;
    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(lat1) * cos(lat2) * sin(dLng / 2) * sin(dLng / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double dist = (earthRadius * c).toDouble();

    return dist;
  }

  static List<double> getPhotoParams(double alt, double focus) {
    double mw = alt * Consts.MATRIX_W / focus;
    double mh = alt * Consts.MATRIX_H / focus;
    return [mw, mh];
  }

  static Map<String, dynamic> getDroneParams() {
    Map<String, dynamic> res = Map();
    List<double> photoP = getPhotoParams(
        droneMaxHeight.value.toDouble(), droneFocusDistance.value.toDouble());
    double photoW = photoP[0];
    double photoH = photoP[1];

    res['photo_w'] = photoW;
    res['photo_h'] = photoH;
    res['charge'] = droneBattery.value;
    res['discharge_flight'] =
        droneBatteryChangeKm.value / 1000; // discharge per metre
    res['discharge_photo'] = droneBatteryChangePhoto.value;
    res['max_height'] = droneMaxHeight.value;
    res['max_alt'] = droneMaxHeight.value;
    res['packs_amount'] = dronePacksAmount.value;
    res['many_packs'] = (droneManyPacks.value == 1) ? true : false;
    res['foc_len'] = droneFocusDistance.value;
    res['photo_overlay'] = dronePhotoOverlay.value;
    res['discharge_km'] = droneBatteryChangeKm.value;
    res['name'] = droneNameController.text.toString();
    return res;
  }

  static void setDroneName(String name) {
    droneNameController.text = name;
    prefs.setString(Consts.PREFS_DRONE_NAME, name);
  }

  static bool get isInDebugMode {
    if (rewriteDebug == "debug") return true;
    if (rewriteDebug == "release") return false;
    bool inDebugMode = false;
    assert(inDebugMode = true);
    return inDebugMode;
  }

  static String getRandomString([int length = 32]) {
    var values = List<int>.generate(length, (i) => rng.nextInt(256));
    return base64Url.encode(values);
  }

  static List<Uint8List> cropRoutingImage(imgBytes) {
    imglib.Image img = imglib.decodeImage(imgBytes);
    Color neededColor = Colors.pink;
    int neededColorValue = neededColor.alpha * 16777216 +
        neededColor.blue * 65536 +
        neededColor.green * 256 +
        neededColor.red;
    int minX = 100000, maxX = 0, minY = 100000, maxY = 0;
    int w = img.width;
    int h = img.height;
    for (int i = 0; i < w; i++) {
      for (int j = 0; j < h; j++) {
        if (img.getPixel(i, j) == neededColorValue) {
          minX = min(minX, i);
          maxX = max(maxX, i);
          minY = min(minY, j);
          maxY = max(maxY, j);
        }
      }
    }
    img = (minX != 100000)
        ? imglib.copyCrop(
            img, minX + 10, minY + 10, maxX - minX - 10, maxY - minY - 10)
        : img;
    w = img.width;
    h = img.height;
    int x = 0, y = 0;
    int ww = min(w, h), hh = min(w, h);
    if (w > h) {
      x = (w - h) ~/ 2;
      y = 0;
    } else {
      x = 0;
      y = (h - w) ~/ 2;
    }
    Uint8List l2 = imglib.encodeJpg(imglib.copyCrop(img, x, y, ww, hh));
    Uint8List l1 = imglib.encodePng(img);
    return [l1, l2];
  }

  static void importSession(String sessionName,
      {Function onReady, Function onError, bool valid = true}) {
    if (!valid) {
      Session resSession = Session();
      List<SessionPhoto> photos = [];
      resSession.name = sessionName;
      resSession.valid = false;
      resSession.routing = false;

      Map<String, dynamic> res = Map();
      res['info'] = Map();
      res['info']['telemetry'] = valid;
      res['info']['fake'] = false;
      res['drone'] = getDroneParams();
      res['drone']['foc_len'] = 8;
      res['photos'] = List();
      for (int i = 0; i < Worker.importFileList.length; i++) {
        Map<String, dynamic> photoMap = Map();
        photoMap['lat'] = photoMap['lng'] = photoMap['alt'] = photoMap['yaw'] =
            photoMap['x1'] = photoMap['y1'] = photoMap['x2'] = photoMap['y2'] =
                photoMap['x3'] =
                    photoMap['y3'] = photoMap['x4'] = photoMap['y4'] = 0;
        photoMap['name'] =
            "f$i${path.extension(Worker.importFileList[i].path)}";
        res['photos'].add(photoMap);
        SessionPhoto sessionPhoto = SessionPhoto();
        sessionPhoto.alt =
            sessionPhoto.lng = sessionPhoto.lat = sessionPhoto.yaw = 0;
        sessionPhoto.name = path.basename(Worker.importFileList[i].path);
        photos.add(sessionPhoto);
      }

      File file = File(path.join(Worker.writeResultsPath, "sessions"));
      if (!file.existsSync()) {
        Directory(file.path).createSync();
      }
      String folderPath = path.join(
          Worker.writeResultsPath, path.join("sessions", "$sessionName"));
      resSession.workingDirectory = folderPath;
      Directory(folderPath).createSync();
      File metaFile = File(path.join(folderPath, 'meta.json'));
      metaFile.writeAsString(json.encode(res));
      for (int i = 0; i < Worker.importList.length; i++) {
        Uint8List image = Worker.importList[i];
        File file = File(path.join(folderPath, res['photos'][i]['name']));
        file.writeAsBytes(image);
      }
      print(Worker.importList[0].length);
      imglib.Image image = imglib
          .getDecoderForNamedImage(path.basename(res['photos'][0]['name']))
          .decodeImage(Worker.importList[0]);
      int ww = image.width, hh = image.height, x = 0, y = 0;
      int w = min(ww, hh), h = min(ww, hh);
      print("$w, $h");
      if (ww > hh) {
        x = (ww - hh) ~/ 2;
        y = 0;
      } else {
        x = 0;
        y = (hh - ww) ~/ 2;
      }
      image = imglib.copyCrop(image, x, y, w, h);
      File thumbnailFile = File(path.join(folderPath, "thumbnail.jpg"));
      thumbnailFile.createSync();
      thumbnailFile.writeAsBytesSync(imglib.encodeJpg(image));

      resSession.photos = photos;
      onReady(resSession);
      return;
    }

    var bytes = Worker.importMetaFile?.readAsBytesSync();
    Session resSession = Session();
    resSession.name = sessionName;
    resSession.routing = false;
    resSession.valid = true;
    List<SessionPhoto> photos = [];
    Worker.fetchParsing(bytes, (response) {
      Map<String, dynamic> decoded = json.decode(response);
      Map<String, dynamic> imgValues = Map();
      for (Map<String, dynamic> photo in decoded['photos']) {
        double phi = degToRadian(-photo['yaw']);
        List<double> photoP = getPhotoParams(photo['alt'], 8);
        double w = photoP[0];
        double h = photoP[1];
        double x0 = photo['lng'];
        double y0 = photo['lat'];
        double x1 = w / 2;
        double y1 = -h / 2;
        double x2 = -w / 2;
        double y2 = -h / 2;
        double x3 = -w / 2;
        double y3 = h / 2;
        double x4 = w / 2;
        double y4 = h / 2;
        double nx1 = x1 * cos(phi) - y1 * sin(phi) + x0;
        double ny1 = y1 * cos(phi) + x1 * sin(phi) + y0;
        double nx2 = x2 * cos(phi) - y2 * sin(phi) + x0;
        double ny2 = y2 * cos(phi) + x2 * sin(phi) + y0;
        double nx3 = x3 * cos(phi) - y3 * sin(phi) + x0;
        double ny3 = y3 * cos(phi) + x3 * sin(phi) + y0;
        double nx4 = x4 * cos(phi) - y4 * sin(phi) + x0;
        double ny4 = y4 * cos(phi) + x4 * sin(phi) + y0;
        photo['x1'] = nx1;
        photo['y1'] = ny1;
        photo['x2'] = nx2;
        photo['y2'] = ny2;
        photo['x3'] = nx3;
        photo['y3'] = ny3;
        photo['x4'] = nx4;
        photo['y4'] = ny4;
        imgValues[path.basenameWithoutExtension(photo['name'])] = photo;
      }
      Map<String, dynamic> res = Map();
      res['photos'] = List();
      for (File file in Worker.importFileList) {
        String fName = basenameWithoutExtension(file.path);
        if (fName.length >= 17) {
          try {
            int a = int.parse(fName.substring(17));
            fName = "f$a";
          } catch (e) {
            onError(e);
          }
        }
        if (!imgValues.containsKey(fName)) {
          onError(response);
          return;
        }
        res['photos'].add(imgValues[fName]);
        SessionPhoto sessionPhoto = SessionPhoto();
        sessionPhoto.name = imgValues[fName]['name'];
        sessionPhoto.yaw = imgValues[fName]['yaw'];
        sessionPhoto.lat = imgValues[fName]['lat'];
        sessionPhoto.lng = imgValues[fName]['lng'];
        sessionPhoto.alt = imgValues[fName]['alt'];
        photos.add(sessionPhoto);
      }
      res['info'] = Map();
      res['info']['telemetry'] = valid;
      res['info']['fake'] = false;
      res['drone'] = getDroneParams();
      res['drone']['foc_len'] = 8;
      File file = File(path.join(Worker.writeResultsPath, "sessions"));
      if (!file.existsSync()) {
        Directory(file.path).createSync();
      }
      String folderPath = path.join(
          Worker.writeResultsPath, path.join("sessions", "$sessionName"));
      Directory(folderPath).createSync();
      resSession.workingDirectory = folderPath;
      resSession.photos = photos;
      File metaFile = File(path.join(folderPath, 'meta.json'));
      metaFile.writeAsString(json.encode(res));
      for (int i = 0; i < Worker.importList.length; i++) {
        Uint8List image = Worker.importList[i];
        File file = File(path.join(folderPath, res['photos'][i]['name']));
        file.writeAsBytes(image);
      }
      imglib.Image image = imglib
          .getDecoderForNamedImage(path.extension(res['photos'][0]['name']))
          .decodeImage(Worker.importList[0]);
      int ww = image.width, hh = image.height, x = 0, y = 0;
      int w = min(ww, hh), h = min(ww, hh);
      if (ww > hh) {
        x = (ww - hh) ~/ 2;
        y = 0;
      } else {
        x = 0;
        y = (hh - ww) ~/ 2;
      }
      image = imglib.copyCrop(image, x, y, w, h);
      File thumbnailFile = File(path.join(folderPath, "thumbnail.jpg"));
      thumbnailFile
        ..createSync()
        ..writeAsBytesSync(imglib.encodeJpg(image));
      onReady(resSession);
    }, (response) {
      onError(response);
    });
  }

  static Future<List> compressParsingPhotos(List<File> fl) async {
    List<Uint8List> newList = List();
    List<File> newFileList = List();
    List res = List();
    for (File file in fl) {
      if (path.extension(file.path) != ".tlog" &&
          path.extension(file.path) != ".json") {
        imglib.Image image = imglib.decodeImage(file.readAsBytesSync());
        imglib.Image compressed =
            image.height > 1200 ? imglib.copyResize(image, width: 1200) : image;
        var bytes = imglib.encodeJpg(compressed, quality: 80);
        newList.add(bytes);
        newFileList.add(file);
      } else
        res.add(file);
    }
    if (res.length == 0) res.add(null);
    res.add(newList);
    res.add(newFileList);
    return res;
  }

  static void readDroneInfo(Map<String, dynamic> meta) async {
    Worker.droneFocusDistance.setValue(meta['foc_len']);
    Worker.dronePacksAmount.setValue(meta['packs_amount']);
    Worker.droneManyPacks.setValue(meta['many_packs'] ? 1 : 0);
    Worker.droneMaxHeight.setValue(meta['max_height']);
    Worker.dronePhotoOverlay.setValue(meta['photo_overlay']);
    Worker.droneBattery.setValue(meta['charge']);
    Worker.droneBatteryChangeKm.setValue(meta['discharge_km']);
    Worker.droneBatteryChangePhoto.setValue(meta['discharge_photo']);
    Worker.droneNameController.text = meta['name'];
  }

  static void fetchRouting(String json,
      {Function onReady, Function onError, Function onLoading}) async {
    await onLoading();
    var postBytes = utf8.encode(json);
    http
        .post(Consts.SERVER_ADDRESS + ":${Consts.PORT_ROUTING}",
            body: postBytes)
        .then((http.Response response) async {
      if (response.statusCode == Consts.STATUS_CODE_OK) {
        String appDocPath = Worker.writeResultsPath;
        String dt = DateTime.now()
            .toIso8601String()
            .replaceAll(":", "-")
            .replaceAll(".", ",");
        File file = File(path.join(appDocPath, "routing_$dt.json"));
        file.writeAsString(response.body);
        onReady(response);
      } else
        onError(response);
    }).catchError((e) {
      print(e);
      onError(e);
    });
  }

  static void fetchComparisonTail(
      Session sessionA,
      Session session1,
      List<File> bytesAList,
      List<File> bytes1List,
      List<String> namesAList,
      List<String> names1List) {
    if (bytesAList.length == 0) return;
    var bytesA = bytesAList[0].readAsBytesSync();
    var bytes1 = bytes1List[0].readAsBytesSync();
    imglib.Decoder decoder = imglib.getDecoderForNamedImage(namesAList[0]);
    var imgA = decoder.startDecode(bytesA);
    var imgAW = imgA.width;
    var imgAH = imgA.height;
    decoder = imglib.getDecoderForNamedImage(names1List[0]);
    var img1 = decoder.startDecode(bytes1);
    var img1W = img1.width;
    var img1H = img1.height;
    _fetchComparison(
      0,
      path.join(sessionA.workingDirectory,
          "compare_${session1.name}_${namesAList[0]}.json"),
      path.join(session1.workingDirectory,
          "compare_${sessionA.name}_${names1List[0]}.json"),
      bytesA,
      bytes1,
      onReady: (resp) {
        Map<String, dynamic> respMap = json.decode(resp);
        List onlyA = respMap['onlyA'];
        List only1 = respMap['only1'];
        List move = respMap['move'];
        List<List<HydratePoint>> pointsA = List();
        List<List<HydratePoint>> points1 = List();
        for (Map<String, dynamic> rect in onlyA) {
          List<HydratePoint> tmp = List();
          tmp.add(HydratePoint(hydrate: 0, x: rect['x'], y: rect['y']));
          tmp.add(
              HydratePoint(hydrate: 0, x: rect['x'], y: rect['y'] + rect['h']));
          tmp.add(HydratePoint(
              hydrate: 0, x: rect['x'] + rect['w'], y: rect['y'] + rect['h']));
          tmp.add(
              HydratePoint(hydrate: 0, x: rect['x'] + rect['w'], y: rect['y']));
          pointsA.add(tmp);
        }
        for (Map<String, dynamic> rect in only1) {
          List<HydratePoint> tmp = List();
          tmp.add(HydratePoint(hydrate: 100, x: rect['x'], y: rect['y']));
          tmp.add(HydratePoint(
              hydrate: 100, x: rect['x'], y: rect['y'] + rect['h']));
          tmp.add(HydratePoint(
              hydrate: 100,
              x: rect['x'] + rect['w'],
              y: rect['y'] + rect['h']));
          tmp.add(HydratePoint(
              hydrate: 100, x: rect['x'] + rect['w'], y: rect['y']));
          points1.add(tmp);
        }
        int k = 1;
        for (Map<String, dynamic> rect in move) {
          List<HydratePoint> tmp = List();
          tmp.add(HydratePoint(
              hydrate: 50, x: rect['xa'], y: rect['ya'], message: "move #$k"));
          tmp.add(HydratePoint(
              hydrate: 50, x: rect['xa'], y: rect['ya'] + rect['ha']));
          tmp.add(HydratePoint(
              hydrate: 50,
              x: rect['xa'] + rect['wa'],
              y: rect['ya'] + rect['ha']));
          tmp.add(HydratePoint(
              hydrate: 50, x: rect['xa'] + rect['wa'], y: rect['ya']));
          pointsA.add(tmp);
          tmp = List();
          tmp.add(HydratePoint(
              hydrate: 50, x: rect['x1'], y: rect['y1'], message: "move #$k"));
          tmp.add(HydratePoint(
              hydrate: 50, x: rect['x1'], y: rect['y1'] + rect['h1']));
          tmp.add(HydratePoint(
              hydrate: 50,
              x: rect['x1'] + rect['w1'],
              y: rect['y1'] + rect['h1']));
          tmp.add(HydratePoint(
              hydrate: 50, x: rect['x1'] + rect['w1'], y: rect['y1']));
          points1.add(tmp);
          k++;
        }

        Worker.tempComparisonList.add([
          PhotoWithResult(
            path.join(sessionA.workingDirectory, namesAList[0]),
            pointsA,
            imgAW,
            imgAH,
          ),
          PhotoWithResult(path.join(session1.workingDirectory, names1List[0]),
              points1, img1W, img1H),
        ]);
        print(bytes1List.length);
        Worker.comparisonController
            .add([Worker.tempComparisonList, (bytes1List.length > 1)]);
        Worker.fetchComparisonTail(
          sessionA,
          session1,
          bytesAList.sublist(1),
          bytes1List.sublist(1),
          namesAList.sublist(1),
          names1List.sublist(1),
        );
      },
      onError: (e) {
        Worker.comparisonController.addError(e);
      },
      onLoading: () {
        print("loading comparison");
      },
    );
  }

  static void tryToMakeComparisonRequest(Session sessionA, Session session1) {
    Worker.tempComparisonList = [];
    Worker.comparisonController.add(null);
    int m = min(sessionA.photos.length, session1.photos.length);
    List<File> bytesAList = [];
    List<File> bytes1List = [];
    List<String> namesAList = [];
    List<String> names1List = [];
    for (int i = 0; i < m; i++) {
      print("PHOTO NAME ${sessionA.photos[i].name}");
      var bytesA =
          File(path.join(sessionA.workingDirectory, sessionA.photos[i].name));
      var bytes1 =
          File(path.join(session1.workingDirectory, session1.photos[i].name));
      bytesAList.add(bytesA);
      bytes1List.add(bytes1);
      namesAList.add(sessionA.photos[i].name);
      names1List.add(session1.photos[i].name);
    }
    fetchComparisonTail(
        sessionA, session1, bytesAList, bytes1List, namesAList, names1List);
  }

  static void tryToMakeHydrationRequest(Session chosenSession) async {
    hydrationResultControllers[chosenSession.name].add(null);
    List<File> files = List();
    List<String> imageNames = List();
    String workingDirectory = chosenSession.workingDirectory;
    for (SessionPhoto photo in chosenSession.photos) {
      files.add(File(path.join(workingDirectory, photo.name)));
      imageNames.add(photo.name);
    }
    Worker.tempHydrationList = [];
    fetchHydrationTail(files, chosenSession);
  }

  static void fetchHydrationTail(List<File> files, Session chosenSession) {
    if (files.length == 0) return;
    var bytes = files[0].readAsBytesSync();
    String workingDirectory = chosenSession.workingDirectory;
    _fetchHydration(workingDirectory, files[0].path, bytes, onError: (a) {
      hydrationResultControllers[chosenSession.name].addError(a);
    }, onReady: (resp) {
      String filePath = resp[0];
      String response = resp[1];
      List<dynamic> list = json.decode(response);
      List<List<HydratePoint>> hydratePoints = List();
      for (Map<String, dynamic> element in list) {
        int hydration = element["hydration"];
        List<HydratePoint> toAdd = List();
        for (Map<String, dynamic> point in element["points"]) {
          toAdd.add(
              HydratePoint(hydrate: hydration, x: point["x"], y: point["y"]));
        }
        hydratePoints.add(toAdd);
      }
      decodeImageFromList(File(filePath).readAsBytesSync())
          .then((decodedImage) {
        Worker.tempHydrationList.add(PhotoWithResult(
            filePath, hydratePoints, decodedImage.width, decodedImage.height));
        hydrationResultControllers[chosenSession.name]
            .add([Worker.tempHydrationList, files.length>1]);
      });
      fetchHydrationTail(files.sublist(1), chosenSession);
    });
  }

  static void _fetchHydration(String workingDirectory, String filePath, bytes,
      {Function onReady, Function onError, Function onLoading}) async {
    File responseFile = File(path.join(workingDirectory,
        "hydration_${path.basenameWithoutExtension(filePath)}.json"));
    if (responseFile.existsSync()) {
      onReady([filePath, responseFile.readAsStringSync()]);
      return;
    } else {
      http
          .post(Consts.SERVER_ADDRESS + ":${Consts.PORT_HYDRATION}",
              body: bytes)
          .then((http.Response response) {
        if (response.statusCode == Consts.STATUS_CODE_OK) {
          responseFile.writeAsString(response.body);
          onReady([filePath, response.body]);
        } else {
          onError(response.reasonPhrase);
        }
      }).catchError(onError);
    }
  }

  static void _fetchComparison(int ind, String responseAPath,
      String response1Path, Uint8List imgABytes, Uint8List img1Bytes,
      {Function onReady, Function onError, Function onLoading}) async {
    File responseFile = File(responseAPath);
    if (responseFile.existsSync()) {
      print("228 13379 exists");
      onReady(responseFile.readAsStringSync());
      return;
    } else {
      var postUri =
          Uri.parse(Consts.SERVER_ADDRESS + ":${Consts.PORT_COMPARISON}");
      var request = http.MultipartRequest("POST", postUri);
      request.files.add(http.MultipartFile.fromBytes(
        'img_a',
        imgABytes,
        filename: "228",
      ));
      request.files.add(http.MultipartFile.fromBytes(
        'img_1',
        img1Bytes,
        filename: "228",
      ));

      request.send().then((http.StreamedResponse response) async {
        print("STATUS CODE ${response.statusCode}");
        if (response.statusCode == 200) {
          Uint8List json = await response.stream.toBytes();
          File writeFile = File(responseAPath);
          await writeFile.writeAsBytes(json);
          writeFile = File(response1Path);
          await writeFile.writeAsBytes(json);
          onReady(utf8.decode(json));
        } else {
          onError(response);
        }
      }).catchError((e) {
        onError(e);
      });
    }
  }

  static Color getHydrationColor(int hydrate, bool showMessages) {
    if (!showMessages) {
      if (hydrate == 0)
        return Color.fromARGB(255, 255, 0, 0);
      else if (hydrate == 1)
        return Color.fromARGB(255, 255, 160, 0);
      else
        return Color.fromARGB(255, 200, 200, 0);
    } else {
      print(hydrate);
      if (hydrate == 0) return Colors.red;
      if (hydrate == 50) return Colors.yellow;
      return Colors.green;
    }
  }

  static Future<String> getMetaFromFile(File file) async {
    return file.readAsString();
  }

  static void tryToMakeCombinationRequest(
      Session chosenSession, bool gif) async {
    if (!gif)
      combinationResultControllers[chosenSession.name].add(null);
    else
      animationResultControllers[chosenSession.name].add(null);
    String workingDirectory = chosenSession.workingDirectory;
    List<File> files = List();
    List<String> imageNames = List();
    for (SessionPhoto photo in chosenSession.photos) {
      files.add(File(path.join(workingDirectory, photo.name)));
      imageNames.add(photo.name);
    }
    files.add(File(path.join(workingDirectory, "meta.json")));

    String meta = "";
    List<Uint8List> photoBytesList = List();
    for (File file in files) {
      if (extension(file.path) == ".json") {
        meta = await getMetaFromFile(file);
        continue;
      }
      var bytes = file.readAsBytesSync();
      photoBytesList.add(bytes);
    }
    if (!gif)
      _fetchCombination(photoBytesList, imageNames, meta, workingDirectory,
          onReady: (response, path) =>
              combinationResultControllers[chosenSession.name].add(path),
          onError: (response) =>
              combinationResultControllers[chosenSession.name]
                  .addError(response));
    else
      _fetchCombination(photoBytesList, imageNames, meta, workingDirectory,
          onReady: (response, path) =>
              animationResultControllers[chosenSession.name].add(path),
          onError: (response) =>
              animationResultControllers[chosenSession.name].addError(response),
          gif: true);
  }

  static void _fetchCombination(List<Uint8List> imagesList,
      List<String> imageNames, String meta, String workingDirectory,
      {Function onReady, Function onError, bool gif = false}) async {
    File responseFile = File(
        path.join(workingDirectory, "combination" + (gif ? ".gif" : ".png")));
    if (responseFile.existsSync()) {
      onReady(http.Response("228", 200), responseFile.path);
      return;
    } else {
      var postUri =
          Uri.parse(Consts.SERVER_ADDRESS + ":${Consts.PORT_COMBINATION}");
      var request = http.MultipartRequest("POST", postUri);
      request.fields['meta'] = meta;
      if (gif) request.fields['gif'] = 'pls';
      int j = 0;
      for (Uint8List list in imagesList) {
        request.files.add(
            http.MultipartFile.fromBytes(imageNames[j], list, filename: "228"));
        //228 сука обязательна!!
        j++;
      }
      request.send().then((http.StreamedResponse response) async {
        if (response.statusCode == 200) {
          Uint8List image = await response.stream.toBytes();
          File writeFile = File(path.join(
              workingDirectory, "combination" + (gif ? ".gif" : ".png")));
          await writeFile.writeAsBytes(image);
          onReady(response, writeFile.path);
        } else {
          onError(response);
        }
      }).catchError((e) {
        onError(e);
      });
    }
  }

  static void fetchParsing(
      Uint8List bytes, Function onReady, Function onError) {
    http
        .post(Consts.SERVER_ADDRESS + ":${Consts.PORT_PARSING}", body: bytes)
        .then((http.Response response) {
      if (response.statusCode == Consts.STATUS_CODE_OK) {
        onReady(response.body);
      } else {
        onError(response.reasonPhrase);
      }
    }).catchError(onError);
  }

  static List parseRouting(String photoPath, String metaFilePath) {
    String metaFileContents = File(metaFilePath).readAsStringSync();
    Map<String, dynamic> map = json.decode(metaFileContents);
    List<MarkerWithTime> resultPoints = List();
    List<DronePhoto> resultPhotoPoints = List();
    Marker droneMarker = Marker(
      width: 45.0,
      height: 45.0,
      anchorPos: AnchorPos.align(AnchorAlign.top),
      point: lat_lng.LatLng(
          map['photos'][0]['lat'] + .0, map['photos'][0]['lng'] + .0),
      builder: (context) => Container(
        child: Icon(
          Icons.location_on,
          color: Colors.blue,
          size: 45,
        ),
      ),
    );
    resultPoints.add(MarkerWithTime(marker: droneMarker, time: 0));
    double time = 0;
    LatLng previousPoint = lat_lng.LatLng(
        map['photos'][0]['lat'] + .0, map['photos'][0]['lng'] + .0);
    for (Map<String, dynamic> photo in map['photos']) {
      droneMarker = Marker(
        width: 45.0,
        height: 45.0,
        anchorPos: AnchorPos.align(AnchorAlign.top),
        point: lat_lng.LatLng(photo['lat'] + .0, photo['lng'] + .0),
        builder: (context) => Container(
          child: Icon(
            Icons.location_on,
            color: Colors.blue,
            size: 45,
          ),
        ),
      );
      time += Worker.distLatLng(previousPoint, droneMarker.point) /
          Consts.DRONE_SPEED;
      resultPoints.add(MarkerWithTime(marker: droneMarker, time: time));
      time += 2;
      lat_lng.LatLng l1, l2, l3, l4;
      try {
        l1 = lat_lng.LatLng(photo['y1'] + .0, photo['x1'] + .0);
        l2 = lat_lng.LatLng(photo['y2'] + .0, photo['x2'] + .0);
        l3 = lat_lng.LatLng(photo['y3'] + .0, photo['x3'] + .0);
        l4 = lat_lng.LatLng(photo['y4'] + .0, photo['x4'] + .0);
      } catch (e) {}
      resultPoints.add(MarkerWithTime(marker: droneMarker, time: time));
      resultPhotoPoints.add(DronePhoto(l1, l2, l3, l4, droneMarker.point,
          path: path.join(photoPath, photo['name'])));
    }
    return [resultPoints, resultPhotoPoints];
  }

  static void saveNewDronePreset(Drone newDrone) {
    String json = newDrone.toString();
    List<String> list = Worker.prefs.getStringList("userDronePresets");
    (list != null) ? list.add(json) : list = [json];
    Worker.prefs.setStringList("userDronePresets", list);
  }

  static void deleteDronePreset(int index) {
    List<String> list = Worker.prefs.getStringList("userDronePresets");
    list.removeAt(index);
    Worker.prefs.setStringList("userDronePresets", list);
  }

  static List<Drone> getDronePresets() {
    List<Drone> res = List.from(Consts.DRONE_PRESETS);
    var stringList = Worker.prefs.getStringList("userDronePresets");
    if (stringList != null)
      res.addAll(stringList.map((s) => Drone.fromJSON(s)));
    return res;
  }
}
