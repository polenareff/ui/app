import 'dart:io';
import 'dart:math';
import 'package:image/image.dart' as imglib;
import 'package:flutter/material.dart';
import 'package:polenareff_app/hydration.dart';
import 'package:polenareff_app/models.dart';

import 'animation.dart';
import 'combination.dart';
import 'comparison.dart';
import 'consts.dart';
import 'mapping.dart';
import 'pole_icon_icons.dart';
import 'package:path/path.dart' as path;
import 'worker.dart';

class DiagnosticsPage extends StatefulWidget {
  DiagnosticsPage(this.chosenSession, {Key key}) : super(key: key);

  final Session chosenSession;

  @override
  _DiagnosticsPageState createState() => _DiagnosticsPageState();
}

class _DiagnosticsPageState extends State<DiagnosticsPage> {
  int _currentTab = 0;

  void _setCurrentTab(int index) {
    setState(() {
      _currentTab = index;
    });
  }

  Widget _getBody() {
    Widget res;
    switch (_currentTab) {
      case Consts.BOTTOM_MAPPING_TAB:
        res = MapPage(widget.chosenSession);
        break;
      case Consts.BOTTOM_HYDRATION_TAB:
        res = HydrationPage(widget.chosenSession,
            key: PageStorageKey(Consts.BOTTOM_HYDRATION_TAB));
        break;
      case Consts.BOTTOM_COMBINATION_TAB:
        res = CombinationPage(widget.chosenSession,
            key: PageStorageKey(Consts.BOTTOM_COMBINATION_TAB));
        break;
      case Consts.BOTTOM_COMPARISON_TAB:
        res = ComparisonPage(widget.chosenSession,
            key: PageStorageKey(Consts.BOTTOM_COMPARISON_TAB));
        break;
      case Consts.BOTTOM_ANIMATION_TAB:
        res = AnimationPage(widget.chosenSession,
            key: PageStorageKey(Consts.BOTTOM_ANIMATION_TAB));
        break;
    }
    return res;
  }

  List<Widget> _getAppBarActions() {
    switch (_currentTab) {
      case Consts.BOTTOM_MAPPING_TAB:
        return [
          IconButton(
            icon: Icon(Icons.remove_red_eye),
            onPressed: ()
            {
              setState(() {
                Worker.mappingShowDrone = !Worker.mappingShowDrone;
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              setState(() {
                Worker.mappingAnimateSpeed += 1;
              });
            },
          ),
          IconButton(
              icon: Icon(Icons.remove),
              onPressed: (Worker.mappingAnimateSpeed >= 1)
                  ? () {
                      setState(() {
                        Worker.mappingAnimateSpeed -= 1;
                      });
                    }
                  : null),
        ];
      default:
        return [];
    }
  }

  Color _getAppFABColor() {
    switch (_currentTab) {
      case Consts.BOTTOM_COMPARISON_TAB:
        if (ComparisonPage.addingState != Consts.STATE_NOTHING)
          return Consts.COLOR_DARK;
        return Consts.COLOR_MAIN;
      default:
        return Consts.COLOR_MAIN;
    }
  }

  Future<bool> _onWillPop() async {
    Worker.isDiagnosticsOpen = false;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    Worker.isDiagnosticsOpen = true;
    if (!widget.chosenSession.valid && _currentTab < 3) _currentTab = 3;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: _getAppFABColor(),
          actions: _getAppBarActions(),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: Image.file(
                      File(
                        path.join(
                          widget.chosenSession.workingDirectory,
                          "thumbnail.jpg",
                        ),
                      ),
                    ).image,
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.chosenSession.name,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                    Text(
                      "${widget.chosenSession.photos.length} photos",
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        body: _getBody(),
        /*floatingActionButton: AnimatedSwitcher(
          duration: Duration(milliseconds: 200), child: _getFAB()),*/
        bottomNavigationBar: SizedBox(
          height: 60,
          child: widget.chosenSession.valid
              ? BottomNavigationBar(
                  currentIndex: _currentTab,
                  type: BottomNavigationBarType.fixed,
                  items: [
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.map,
                        size: 30,
                      ),
                      title: Text("Map"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.photo_library,
                        size: 30,
                      ),
                      title: Text("Combine"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        PoleIcon.gif,
                        size: 30,
                      ),
                      title: Text("Animate"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.compare,
                        size: 30,
                      ),
                      title: Text("Compare"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        PoleIcon.drop,
                        size: 30,
                      ),
                      title: Text("Hydrate"),
                    ),
                  ],
                  onTap: (index) => _setCurrentTab(index),
                )
              : BottomNavigationBar(
                  currentIndex: max(_currentTab - 3, 0),
                  type: BottomNavigationBarType.fixed,
                  items: [
                    BottomNavigationBarItem(
                      icon: Icon(
                        Icons.compare,
                        size: 30,
                      ),
                      title: Text("Compare"),
                    ),
                    BottomNavigationBarItem(
                      icon: Icon(
                        PoleIcon.drop,
                        size: 30,
                      ),
                      title: Text("Hydrate"),
                    ),
                  ],
                  onTap: (index) {
                    _setCurrentTab(index + 3);
                  },
                ),
        ),
      ),
    );
  }
}
