import 'dart:ui';

import 'package:latlong/latlong.dart';

import 'models.dart';

class Consts {
  static const double MAP_MIN_ZOOM = 6;
  static const double MAP_MAX_ZOOM = 20;

  static const double MATRIX_W = 12.8;
  static const double MATRIX_H = 9.6;

  static const String PREFS_DRONE_NAME = "drone_name";
  static const int STATE_NOTHING = 0;
  static const int STATE_ADDING = 1;
  static const int STATE_PROCESSING = 2;
  static const int STATE_MAKING_PHOTO = 3;
  static const double DRONE_SPEED = 25; //metre в секунду
  static const Duration DRONE_FRAME_DURATION = Duration(milliseconds: 40);
  static const int BOTTOM_TABS = 5;
  static const int BOTTOM_MAPPING_TAB = 0;
  static const int BOTTOM_COMBINATION_TAB = 1;
  static const int BOTTOM_ANIMATION_TAB = 2;
  static const int BOTTOM_COMPARISON_TAB = 3;
  static const int BOTTOM_HYDRATION_TAB = 4;
  static const Color COLOR_MAIN = Color(0xFF46A04F);
  static const Color COLOR_DARK = Color(0xFF35783b);
  static const int COLOR_MAIN_INT = 0xFF46A04F;
  static const int PORT_ROUTING = 5000;
  static const int PORT_HYDRATION = 6000;
  static const int PORT_COMBINATION = 7007;
  static const int PORT_PARSING = 7331;
  static const int PORT_COMPARISON = 6666;
  static const int STATUS_CODE_OK = 200;
  static const String MESSAGE_MAKING_DRONE_PHOTOS = ""
      "Creating new session...";
  static const String MESSAGE_MADE_DRONE_PHOTOS = ""
      "The new session was created!";
  static const String MESSAGE_CANCELED_MAKING_DRONE_PHOTOS = ""
      "Canceled creating session!";
  static const String MESSAGE_ROUTING_FUTURE_REQUEST =
      "The routing request will be sent in a few seconds";
  static const String MESSAGE_ROUTING_ERROR = ""
      "Error happened while processing routing request";
  static const String MESSAGE_ROUTING_OK = ""
      "Routing request has been successfully proccessed";
  static const String MESSAGE_ROUTING_PROCESSING = ""
      "Processing routing request..";

  static const String MESSAGE_HYDRATION_FUTURE_REQUEST =
      "The hydration request will be sent in a few seconds";
  static const String MESSAGE_HYDRATION_ERROR = ""
      "Error happened while processing hydration request";
  static const String MESSAGE_HYDRATION_OK = ""
      "Hydration request has been successfully proccessed";
  static const String MESSAGE_HYDRATION_PROCESSING = ""
      "Processing hydration request..";

  static const String MESSAGE_COMBINATION_FUTURE_REQUEST =
      "The combination request will be sent in a few seconds";
  static const String MESSAGE_COMBINATION_ERROR = ""
      "Error happened while processing combination request";
  static const String MESSAGE_COMBINATION_OK = ""
      "Combination request has been successfully proccessed";
  static const String MESSAGE_COMBINATION_PROCESSING = ""
      "Processing combination request..";

  static const String MESSAGE_ANIMATION_FUTURE_REQUEST =
      "The animation request will be sent in a few seconds";
  static const String MESSAGE_ANIMATION_ERROR = ""
      "Error happened while processing animation request";
  static const String MESSAGE_ANIMATION_OK = ""
      "Animation request has been successfully proccessed";
  static const String MESSAGE_ANIMATION_PROCESSING = ""
      "Processing animation request..";
  //пардон, но константы не сегодня
  // ignore: non_constant_identifier_names
  static String SERVER_ADDRESS = "http://35.207.174.36";
  // ignore: non_constant_identifier_names
  static LatLng MAPPING_DEFAULT_LATLNG = LatLng(50.135813, 35.675694);
  // ignore: non_constant_identifier_names
  static double MAP_VALUE_ZOOM = 6;
  static List<Drone> DRONE_PRESETS = [
    Drone(
      maxHeight: 120,
      batteryChangeKm: 450,
      batteryChangePhoto: 50,
      focusDistance: 14,
      battery: 5400,
      name: "Yneec Q50",
    ),
    Drone(
      maxHeight: 500,
      batteryChangeKm: 375,
      batteryChangePhoto: 40,
      focusDistance: 20,
      battery: 5100,
      name: "Xoaimi Drone",
    ),
    Drone(
      maxHeight: 120,
      batteryChangeKm: 320,
      batteryChangePhoto: 80,
      focusDistance: 14,
      battery: 5400,
      name: "Ynecc Typhoon QH",
    ),
    Drone(
      maxHeight: 500,
      batteryChangeKm: 480,
      batteryChangePhoto: 70,
      focusDistance: 30,
      battery: 5100,
      name: "Karma",
    ),
  ];
}
