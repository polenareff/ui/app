import 'dart:io';

import 'package:extended_image/extended_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:path/path.dart' as path;
import 'package:polenareff_app/choose_session.dart';
import 'package:rxdart/rxdart.dart';

import 'consts.dart';
import 'image_viewer.dart';
import 'menu.dart';
import 'models.dart';
import 'worker.dart';

class ComparisonPage extends StatefulWidget {
  ComparisonPage(this.chosenSession, {Key key});

  Session chosenSession = Session();
  static int addingState = Consts.STATE_NOTHING;
  List<bool> showFirst = [];

  static void setNothingState() {
    addingState = Consts.STATE_NOTHING;
  }

  @override
  _ComparisonPageState createState() => _ComparisonPageState();
}

class _ComparisonPageState extends State<ComparisonPage> {
  Widget getSessionList(List<Session> sessions) {
    if (sessions.length == 0) {
      return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.5,
                child: Image.asset("assets/empty.png"),
              ),
              SizedBox(height: 20),
              Text(
                "You haven't created any session yet!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 50),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    child: Text("Import session"),
                    onPressed: () {
                      MenuPage.openImportPage(context, replace: true);
                    },
                  ),
                  SizedBox(width: 30),
                  RaisedButton(
                    child: Text("Build route"),
                    onPressed: () {
                      MenuPage.openRoutingPage(context, replace: true);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
    return ListView.separated(
      separatorBuilder: (context, index) => Padding(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
        child: Divider(
          color: Color.fromARGB(255, 200, 200, 200),
        ),
      ),
      itemCount: sessions.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            setState(() {
              Worker.comparingSession = sessions[index];
              Worker.tryToMakeComparisonRequest(
                  widget.chosenSession, Worker.comparingSession);
            });
          },
          child: Container(
            height: 220,
            color: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.only(bottom: 10, top: 10),
              child: Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 7, 0, 7),
                    child: Image.file(
                      File(
                        path.join(
                            sessions[index].workingDirectory, "thumbnail.jpg"),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 70),
                        Text(
                          sessions[index].name,
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 15),
                        Text("${sessions[index].photos.length} photos"),
                      ],
                    ),
                  ),
                  Spacer(flex: 1),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (Worker.comparingSession == null) {
      List<Session> sessions = ChooseSession.getSessions();
      for (int i = 0; i < sessions.length; i++) {
        if (sessions[i].name == widget.chosenSession.name) {
          sessions.removeAt(i);
          break;
        }
      }
      return getSessionList(sessions);
    }
    return StreamBuilder(
      stream: Worker.comparisonController.stream,
      builder: (context, snapshot) {
        print(snapshot);
        if (snapshot.hasError) {
          return Column(
            children: [
              Spacer(),
              Center(
                child: Text(
                  "Error happened!",
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
              ),
              SizedBox(height: 5),
              Center(
                child: Text(
                  snapshot.error.toString(),
                ),
              ),
              SizedBox(height: 20),
              RaisedButton(
                child: Text("Reload comparison request"),
                onPressed: () {
                  Worker.tryToMakeComparisonRequest(
                      widget.chosenSession, Worker.comparingSession);
                },
              ),
              Spacer(),
            ],
          );
        } else if (snapshot.hasData) {
          List resultPhotos = snapshot.data[0];
          bool showProgress = snapshot.data[1];
          for (int i = widget.showFirst.length; i < resultPhotos.length; i++)
            widget.showFirst.add(true);
          return ExtendedImageGesturePageView.builder(
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    widget.showFirst[index] = !widget.showFirst[index];
                  });
                },
                child: (index != resultPhotos.length)
                    ? PhotoMap(
                        File(resultPhotos[index]
                                [widget.showFirst[index] ? 1 : 0]
                            .path),
                        resultPhotos[index][widget.showFirst[index] ? 1 : 0]
                            .hydratePoints,
                        resultPhotos[index][widget.showFirst[index] ? 1 : 0]
                            .width,
                        resultPhotos[index][widget.showFirst[index] ? 1 : 0]
                            .height,
                        true,
                        showPercents: false,
                        showMessages: true,
                      )
                    : Center(child: CircularProgressIndicator(),)
              );
            },
            itemCount: resultPhotos.length+(showProgress ? 1 : 0),
            controller: PageController(
              initialPage: 0,
            ),
            scrollDirection: Axis.horizontal,
          );
        }
        return Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Spacer(flex: 1),
              CircularProgressIndicator(),
              Spacer(flex: 1),
            ],
          ),
        );
      },
    );
  }
}
