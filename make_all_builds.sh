#!/bin/bash
flutter clean
rm -rf go/build/
hover build -t lib/main_desktop_release.dart darwin
rm -rf go/build/outputs/darwin/flutter_assets/go
hover build -t lib/main_desktop_release.dart linux
rm -rf go/build/outputs/linux/flutter_assets/go
hover build -t lib/main_desktop_release.dart windows
rm -rf go/build/outputs/windows/flutter_assets/go
flutter build apk -t lib/main_release.dart 
