#!/bin/bash
flutter clean
rm -rf go/build/
hover build -t lib/main_desktop.dart darwin
rm -rf go/build/outputs/darwin/flutter_assets/go
hover build -t lib/main_desktop.dart linux
rm -rf go/build/outputs/linux/flutter_assets/go
hover build -t lib/main_desktop.dart windows
rm -rf go/build/outputs/windows/flutter_assets/go
flutter build apk --debug -t lib/main.dart 
